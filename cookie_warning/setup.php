<?php
/**
 * plugins/cookie_warning/setup.php -- Main setup script
 *
 * SquirrelMail Cookie Warning Plugin
 * Copyright (C) 2004-2005 Tomas Kuliavas <tokul@users.sourceforge.net>
 * This program is licensed under GPL. See COPYING for details
 *
 * @version $Id: setup.php,v 1.7 2005/04/11 08:27:02 tokul Exp $
 * @package sm-plugins
 * @subpackage cookie_warning
 */

/** @ignore */
if (!defined('SM_PATH')) {
    define('SM_PATH','../../');
}

/**
 * init function
 */
function squirrelmail_plugin_init_cookie_warning() {
  global $squirrelmail_plugin_hooks;

  // set cookie on login page
  $squirrelmail_plugin_hooks['login_cookie']['cookie_warning'] = 'cookie_warning_cookie';
  // if login fails, check if cookie was set and modify logout message.
  $squirrelmail_plugin_hooks['logout_error']['cookie_warning'] = 'cookie_warning_onlogout';
}

/**
 * hooks up cookie warning to logout_error hook (functions/display_messages.php)
 * @since 1.1
 */
function cookie_warning_onlogout(&$hook_args) {
  include_once(SM_PATH . 'plugins/cookie_warning/functions.php');
  cookie_warning_onlogout_function($hook_args);
}

/**
 * sets own cookie in login page.
 * @since 1.1
 */
function cookie_warning_cookie() {
  include_once(SM_PATH . 'plugins/cookie_warning/functions.php');
  cookie_warning_cookie_function();
}

/**
 * shows plugin's version
 * @return string
 */
function cookie_warning_version() {
  return '1.1';
}
?>