<?php
/**
 *  plugins/cookie_warning/index.php -- Displays the main frameset
 *
 * SquirrelMail Cookie Warning Plugin
 * Copyright (C) 2004-2005 Tomas Kuliavas <tokul@users.sourceforge.net>
 * This program is licensed under GPL. See COPYING for details
 *
 * Redirects to the login page.
 *
 * @version $Id: index.php,v 1.3 2005/03/27 08:18:53 tokul Exp $
 * @package sm-plugins
 * @subpackage cookie_warning
 */
header("Location: ../../src/index.php.php\n\n");
exit();
?>
