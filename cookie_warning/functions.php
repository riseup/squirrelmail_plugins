<?php
/**
 * plugins/cookie_warning/functions.php -- Functions used by plugin
 *
 * SquirrelMail Cookie Warning Plugin
 * Copyright (C) 2004-2005 Tomas Kuliavas <tokul@users.sourceforge.net>
 * This program is licensed under GPL. See COPYING for details
 *
 * @version $Id: functions.php,v 1.4 2005/03/27 08:18:53 tokul Exp $
 * @package sm-plugins
 * @subpackage cookie_warning
 */

/** @ignore */
if (!defined('SM_PATH')) {
    define('SM_PATH','../../');
}

/* get sqm_baseuri */
include_once(SM_PATH . 'functions/display_messages.php');

/** globals */
global $cw_use_own_cookie;

/** load default config */
if (file_exists(SM_PATH . 'plugins/cookie_warning/config_default.php')) {
    include_once(SM_PATH . 'plugins/cookie_warning/config_default.php');
} else {
    // somebody removed default config.
    $cw_use_own_cookie=true;
}

/** load site config */
if (file_exists(SM_PATH . 'config/cookie_warning_config.php')) {
    include_once(SM_PATH . 'config/cookie_warning_config.php');
} elseif (file_exists(SM_PATH . 'plugins/cookie_warning/config.php')) {
    include_once(SM_PATH . 'plugins/cookie_warning/config.php');
}

/**
 * Detects if session cookie is set in logout_error function.
 * Minimal requirement: SquirrelMail 1.4.3.
 * @param array $hook_args array with hook arguments 
 * (0 = hook name, 1 = error string, 2 = error page title)
 * @since 1.1
 */
function cookie_warning_onlogout_function(&$hook_args) {
    global $session_name, $cw_use_own_cookie;

    $errString=&$hook_args[1];
    $errTitle=&$hook_args[2];

    if ($cw_use_own_cookie) {
        $cookie_check=sqgetGlobalVar('sqm_cookie_check',$cookie,SQ_COOKIE);
    } else {
        $cookie_check=sqgetGlobalVar($session_name,$cookie,SQ_COOKIE);
    }

    if (! $cookie_check) {
        // Set domain
        bindtextdomain ('cookie_warning', SM_PATH . 'locale');
        textdomain ('cookie_warning');
        $errString =  _("This interface requires cookie support in browser.");
        $errTitle = _("Cookie Warning");
        // Revert domain
        bindtextdomain ('squirrelmail', SM_PATH . 'locale');
        textdomain ('squirrelmail');
    }
}

/**
 * sets own cookie in login page.
 * @since 1.1
 */
function cookie_warning_cookie_function() {
    global $cw_use_own_cookie;
    if ($cw_use_own_cookie) {
        setcookie('sqm_cookie_check',time(),0,sqm_baseuri());
    }
}
?>