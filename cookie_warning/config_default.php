<?php
/**
 * Default cookie_warning configuration file
 * Copyright (C) 2005 Tomas Kuliavas <tokul@users.sourceforge.net>
 * This program is licensed under GPL. See COPYING for details
 * @version $Id: config_default.php,v 1.1 2005/03/27 08:18:53 tokul Exp $
 * @package sm-plugins
 * @subpackage cookie_warning
 * @since 1.1
 */

/**
 * controls use of own cookie for cookie support detection
 * Uses session cookie, if set to false.
 * @global boolean $cw_use_own_cookie
 */
$cw_use_own_cookie=true;
?>