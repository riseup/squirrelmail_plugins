Plug-in name: Anti Fixation Attacks
Short name: antifixation
Description: Prevents session fixation attacks
Author: elijah@riseup.net
Version: 1.0
Date: Mar 11 2006

INTRODUCTION

A session fixation attack is when the attacker is able to
'seed' a particular session id in the user's browser. The
attacker can do this in a variety of ways, most of which
require that they are able to post custom html on a similar
domain to the squirrelmail login page. When the user views
this custom html, their browser can be tricked into adopting
a predetermined session id picked by the attacker.

Here is a trivial example of some fixating html:

<meta http-equiv="Set-Cookie" content="SQMSESSID=31d857c52046d2e418ea5f24675e5180;domain=.riseup.net;path=/">

Once the user has adopted this predetermined session id, if
the user then logs into squirrelmail the attacker will be
able to access their account. This is because the attacker
has a cookie in their browser which exactly matches the
cookie in the user's browser. Squirrelmail will treat both
the attacker and the user as the same client with the same
valid session. SSL does nothing to prevent this.

This plugin attempts to banish such attacks by ensuring that
the session id is created anew each and every time there is
a successful authentication. This has the added benefit of
preventing a user from seeing the cached session data of
another user who forgot to logout while using the same
computer (it can be confusing to send mail with the other
user's return address!).

And, as always, if you care anything about security, make
sure that the sessions are only using cookies and not URL
rewriting (ie session.use_only_cookies = 1).

To understand session fixation attacks, a good primer is:
http://www.acros.si/papers/session_fixation.pdf

NOTES

In order to prevent session fixation, we must make sure we
delete all the possible session cookies which might exist
and which would take priority over our new one. This turns
out to be difficult: php will fall back to the session
cookie with the shortest path and shortest domain name.
Since an attacker would pick this type of cookie, we are
forced into creating our session cookie with an equally
global scope.

In theory, we should be able to delete all the possible
cookies with a more global scope than the one we create.
This way, we could use a cookie with a path consistant with
the squirrelmail configuration.

Unfortunately, squirrelmail has a small bug: on the login
page, there is a session cookie set with a root
path--irrespective of what the session path should be. Later
code sets a path for the session cookie: this is all fine
and well, but if a root path session cookie is created
first, then these others are not used in practice.

this initial bad session is created from:

 login.php --> include(strings.php) --> 
     include(global.php) --> sqsession_is_active();

This session is created before:

  $base_uri = sqm_baseuri()
  session_set_cookie_params(0, $base_uri)

This means that we are actually stuck using a root path
session cookie, no matter what the code elsewhere seems to
think. If we chose to follow squirrelmail and create
session cookie with a path, the root path cookie would
always take precidence on the next page refresh over our
newly created session cookies.

PHP appears to adopt whatever path is set in an available
session if you don't explicitly call
session_set_cookie_params before starting the session.


CHANGES

1.0  Thu Mar 16 2006
     Original release
