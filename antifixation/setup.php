<?php
/* Version 1.0 */

function squirrelmail_plugin_init_antifixation() {
	global $squirrelmail_plugin_hooks;
	$squirrelmail_plugin_hooks['login_verified']['antifixation'] = 'plugin_antifixation_login_verified';
}

function plugin_antifixation_login_verified() {
	## destroy all existing cookies with a matching session name.
	## (on all possible domains and all possible paths)
	if (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time()-42000, '/');
		$shorthost = preg_replace('/^.*(\..*\..*)$/','$1',$_SERVER['HTTP_HOST']);  // convert mail.riseup.net -> .riseup.net
		if ($shorthost{0} != '.') $shorthost = ".$shorthost";                      // ensure leading "."
		setcookie(session_name(), '', time()-42000, '/', $shorthost);
	}
	## copy the current data
	$oldsession = $_SESSION;

	## destroy all the data in the current session.
	session_destroy();

	## create a new session, with a different id
	session_set_cookie_params(0, '/'); // <-- this is required in case an existing session cookie has a root path.
	session_start();
	session_regenerate_id(); 

	## copy back the data which is useful for a new login
	$keep = array('sq_base_url', 'base_uri','onetimepad','sqimap_capabilities','delimiter','username');
	foreach($keep as $varname) {
 		if (isset($oldsession[$varname]))
			$_SESSION[$varname] = $oldsession[$varname];
	}
}
?>
