<?php
/* jump_to_folder -- Version 0.3
 * By Robin Rainton <robin@rainton.com>
 * http://rainton.com/fun/freeware
 *
 * Utilise the "folders_bottom" hook to display
 * drop down of folders to jump to.
 */

function squirrelmail_plugin_init_jump_to_folder() {
  global $squirrelmail_plugin_hooks;
  $squirrelmail_plugin_hooks['left_main_after']['jump_to_folder'] =
    'jump_to_folder_list';
  $squirrelmail_plugin_hooks['right_main_after_header']['jump_to_folder'] =
    'jump_to_folder_on_menu';
  $squirrelmail_plugin_hooks['options_folder_inside']['jump_to_folder'] =
    'jump_to_folder_options';
  $squirrelmail_plugin_hooks['options_folder_save']['jump_to_folder'] =
    'jump_to_folder_save';
  $squirrelmail_plugin_hooks['loading_prefs']['jump_to_folder'] =
    'jump_to_folder_pref';
}

/*
 * Function that is called when the menu is drawn, display the
 * drop down there is this option is on.
 */

function jump_to_folder_on_menu()
{
  global $jump_to_folder_show_mailbox, $boxes, $imapConnection;
/*
 * Forget it if the option isn't turned on.
 */

  if (!isset($jump_to_folder_show_mailbox) || !$jump_to_folder_show_mailbox) return;

/*
 * Get the mailbox list if we haven't already.
 */

  if (!isset($boxes)) {
    $boxes = sqimap_mailbox_list($imapConnection);
  }

  jump_to_folder_build($boxes);
}

/*
 * Function that is called when the folder list is drawn, display the
 * drop down there is this option is on.
 */

function jump_to_folder_list()
{
  global $jump_to_folder_show, $jump_to_folder_only_collapsed;
  global $boxes;

/*
 * Forget it if the option isn't turned on.
 */

  if (!isset($jump_to_folder_show) || !$jump_to_folder_show) return;

 /*
  * Don't bother if nothing is collapsed (and that option is on)
  */

  if (isset($jump_to_folder_only_collapsed) && $jump_to_folder_only_collapsed) {
    $show = FALSE;
    for ($lp = 0; $lp < count($boxes) && !$show; $lp++) {
      if ($boxes[$lp]['collapse'] == SM_BOX_COLLAPSED) $show = TRUE;
    }
    if (!$show) return;
  }

  jump_to_folder_build($boxes);
}

/*
 * The next function builds a list of folders and displays
 * the drop down if that option is on.
 */

function jump_to_folder_build($boxes)
{
/*
 * OK - show dropdown.
 */

  ?><center><form action="right_main.php" target="right">
<input type="hidden" name="PG_SHOWALL" value="0">
<input type="hidden" name="sort" value="0">
<input type="hidden" name="startmessage" value="1">
<select name="mailbox" class="small" onchange="submit();"><option value="INBOX">Jump to...</option>
<?php

  $lastbox = "";
  for ($lp = 0; $lp < count($boxes); $lp++) {
    $mailbox = $boxes[$lp]['formatted'];

    if (!in_array('noselect', $boxes[$lp]['flags'])) {
      echo '<option value="' . $boxes[$lp]['unformatted'] . '">' .
           $mailbox . "</option>\n";
    } else {
      echo '<option value="INBOX">' .
           $mailbox . "</option>\n";
    }
  }
  ?></select>
<noscript><BR><input type="submit" value="Go" class="small"></noscript>
</form></center><?php
}

/*
 * Here come the few functions for preference handling.
 */

function jump_to_folder_options()
{
  global $jump_to_folder_show, $jump_to_folder_only_collapsed,
         $jump_to_folder_show_mailbox;
  
  ?><tr><td align=right nowrap valign="middle">Show Jump to Folder Dropdown:</td><td>
    <INPUT TYPE="RADIO" NAME="jump_to_folder_opt_show" VALUE="1"<?PHP
      if (isset($jump_to_folder_show) && $jump_to_folder_show)
        echo ' CHECKED';
    ?>>&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT TYPE="RADIO" NAME="jump_to_folder_opt_show" VALUE="0"<?PHP
      if (!isset($jump_to_folder_show) || !$jump_to_folder_show)
        echo ' CHECKED';
    ?>>&nbsp;No
  </td></tr><tr><td align=right nowrap valign="middle">Only Show if Some Folders Collapsed:</td><td>
    <INPUT TYPE="RADIO" NAME="jump_to_folder_opt_collapsed" VALUE="1"<?PHP
      if (isset($jump_to_folder_only_collapsed) && $jump_to_folder_only_collapsed)
        echo ' CHECKED';
    ?>>&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT TYPE="RADIO" NAME="jump_to_folder_opt_collapsed" VALUE="0"<?PHP
      if (!isset($jump_to_folder_only_collapsed) || !$jump_to_folder_only_collapsed)
        echo ' CHECKED';
    ?>>&nbsp;No
  </td></tr><tr><td align=right nowrap valign="middle">Always Show Above Mailbox Display:</td><td>
    <INPUT TYPE="RADIO" NAME="jump_to_folder_opt_show_mailbox" VALUE="1"<?PHP
      if (isset($jump_to_folder_show_mailbox) && $jump_to_folder_show_mailbox)
        echo ' CHECKED';
    ?>>&nbsp;Yes&nbsp;&nbsp;&nbsp;&nbsp;
        <INPUT TYPE="RADIO" NAME="jump_to_folder_opt_show_mailbox" VALUE="0"<?PHP
      if (!isset($jump_to_folder_show_mailbox) || !$jump_to_folder_show_mailbox)
        echo ' CHECKED';
    ?>>&nbsp;No
  </td></tr><?PHP
}

function jump_to_folder_save()
{
  global $username, $data_dir;

  setPref($data_dir, $username, "jump_to_folder_show",
          isset($_POST['jump_to_folder_opt_show']) ?
            $_POST['jump_to_folder_opt_show'] : "1");
  setPref($data_dir, $username, "jump_to_folder_only_collapsed",
          isset($_POST['jump_to_folder_opt_collapsed']) ?
            $_POST['jump_to_folder_opt_collapsed'] : "1");
  setPref($data_dir, $username, "jump_to_folder_show_mailbox",
          isset($_POST['jump_to_folder_opt_show_mailbox']) ?
            $_POST['jump_to_folder_opt_show_mailbox'] : "0");
}

function jump_to_folder_pref()
{ 
  global $username, $data_dir;
  global $jump_to_folder_show, $jump_to_folder_only_collapsed,
         $jump_to_folder_show_mailbox;

  $jump_to_folder_show = getPref($data_dir, $username, 'jump_to_folder_show', '1');
  $jump_to_folder_only_collapsed = getPref($data_dir, $username, 'jump_to_folder_only_collapsed', '1');
  $jump_to_folder_show_mailbox = getPref($data_dir, $username, 'jump_to_folder_show_mailbox', '0');
}

?>
