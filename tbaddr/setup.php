<?php

function squirrelmail_plugin_init_tbaddr() {
	global $squirrelmail_plugin_hooks;

	$squirrelmail_plugin_hooks['compose_bottom']['tbaddr']               = 'tbaddr_compose_bottom';
	$squirrelmail_plugin_hooks['loading_prefs']['tbaddr']                = 'tbaddr_loading_prefs';
	$squirrelmail_plugin_hooks['generic_header']['tbaddr']               = 'tbaddr_generic_header';
	$squirrelmail_plugin_hooks['optpage_register_block']['tbaddr']       = 'tbaddr_optpage_register_block';
}   
   
function tbaddr_optpage_register_block() {
  global $optpage_blocks;
		if (function_exists('nsm_bindtextdomain')) {
			nsm_bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
			nsm_textdomain('tbaddr');
		} else {
			bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
			textdomain('tbaddr');
		}
		$optpage_blocks[] =
				array(
						'name' => _("Address Autocompletion Options"),
						'url'  => '../plugins/tbaddr/options.php',
						'desc' => _("Here you may set up your preferences for address autocompletion."),
						'js'   => TRUE);
		if (function_exists('nsm_bindtextdomain')) {
			nsm_bindtextdomain('nasmail',SM_PATH . 'locale');
			nsm_textdomain('nasmail');
		} else {
			bindtextdomain('squirrelmail',SM_PATH . 'locale');
			textdomain('squirrelmail');
		}
}

function tbaddr_generic_header() {
	$page=basename($_SERVER['PHP_SELF']);
	include_once(SM_PATH . 'plugins/tbaddr/functions.php');
	switch ($page) {
		case 'addrbook_search.php':
		case 'abook_group_interface.php':
			tbaddr_abook_init_do();
			break;
		case 'compose.php':
			global $toptions;
			tbaddr_loadprefs();
			?>
			<style type="text/css">
			.TBADiv {
				position: relative;
				overflow: visible;
			}
			.TBADiv input, .TBADiv img {
				vertical-align: middle;
			}
			.TBIFrame {
				position: absolute;
				top: 21px;
				left: 0px;
				border: none;
				display: block;
				visibility: hidden;
			}
			.TBBDiv {
				text-align: left;
				color: black;
				position: absolute;
				left: 0px;
				top: 21px;
				border: solid black 1px;
				visibility: hidden;
				cursor: default;
			}
			.TBNormal, .TBSelected {
				color: <?php echo $toptions['textClr']?>;
				padding: 2px;
			}
			.TBNormal span, .TBSelected span {
				color: <?php echo $toptions['mtextClr']?>;
				font-weight: bold;
			}
			.TBNormal {
				background-color: <?php echo $toptions['bgClr']?>;
			}
			.TBSelected {
				background-color: <?php echo $toptions['selAddrClr']?>;
			}
			</style>
			<?php
			break;
		case 'options.php':
			?>
			<style type="text/css">
			.cptbl, .cptbl td, .cptbl input {
				padding: 0px;
			}
			.cptbl input {
				border: 1px solid black;
				height: 19px;
			}
			.cptbl input.ff {
				font-family: Monospace;
				font-weight: 12pt;
			}
			</style>
			<?php
			break;
		default:
//			echo 'xxxx('.$page.')xxxx';
	}
}

function tbaddr_compose_bottom() {
	include_once(SM_PATH . 'plugins/tbaddr/functions.php');

	tbaddr_compose_bottom_do();
}

function tbaddr_loading_prefs() {
	include_once(SM_PATH . 'plugins/tbaddr/functions.php');

	tbaddr_loadprefs();
}

function tbaddr_info() {
	return array(
		'english_name' => 'Thunderbird Style Address Completion',
		'version' => '0.92',
		'required_sm_vesion' => '1.4.0', 
		'summary' => 'Autocompletes addresses while typing.',
		'details' => 'This adds the ability to use an address completion that is modeled after the Thunderbird email client.',
		'requires_configuration' => 0,
		'requires_source_patch' => 0,
	);
}

function tbaddr_version() {
	$info = tbaddr_info();
	return $info['version'];
}