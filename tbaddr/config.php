<?php
//Default options(1=on, 0=off)
//These are the settings that will be used by default for a new user, they can be overridden by each user
//Enable plugin
$enable_autocomplete       = 1;
//Start with an empty CC address field
$emptycc_autocomplete      = 1;
//Start with an empty BCC address field
$emptybcc_autocomplete     = 1;
//Use personal address book
$personalbook_autocomplete = 1;
//Use global address book
$globalbook_autocomplete   = 1;
//Complete on full name
$name_autocomplete         = 1;
//Complete on nickname
$nickname_autocomplete     = 1;
//Complete on email
$email_autocomplete        = 1;
//x characters before search
$minlen_autocomplete       = 1;
//Complete on full name
$ldapname_autocomplete     = 1;
//Complete on nickname
$ldapnickname_autocomplete = 0;
//Complete on email
$ldapemail_autocomplete    = 1;
//x characters before search
$ldapminlen_autocomplete   = 1;
//Dropdown colors
$bgClr_autocomplete        = '#FFFFFF';
$textClr_autocomplete      = '#000000';
$mtextClr_autocomplete     = '#0000FF';
$selAddrClr_autocomplete   = '#D0D0CC';
//Case insensitive
$insensitive_autocomplete  = 1;
//Show dropdown of possible matches
$dropdown_autocomplete     = 1;
//Enable LDAP
$ldap_autocomplete         = 0;
//Enter the nickname field for your LDAP server to enable LDAP nicknames
//must match index for associated LDAP server in /config/config.php
$ldapnicknames = array();
//$ldapnicknames[0]         = "uid";