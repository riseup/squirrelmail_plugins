<?php
function php2js($a) {
// return json_encode($a);
	if (is_null($a)) return 'null';
	if ($a === false) return 'false';
	if ($a === true) return 'true';
	if (is_scalar($a)) {
		if (is_float($a)) {
			// Always use "." for floats.
			$a = str_replace(",", ".", strval($a));
		}

		static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'),
		array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
		if(is_integer($a)) return str_replace($jsonReplaces[0], $jsonReplaces[1], $a);
		else return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
	}
	$isList = true;
	for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
		if (key($a) !== $i) {
			$isList = false;
			break;
		}
	}
	$result = array();
	if ($isList) {
		foreach ($a as $v) $result[] = php2js($v);
		return '[ ' . join(', ', $result) . ' ]';
	} else {
		foreach ($a as $k => $v) $result[] = php2js($k).': '.php2js($v);
		return '{ ' . join(', ', $result) . ' }';
	}
}

function tbaddr_compose_bottom_do() {
	global $location_of_buttons,$ldap_server;
	global $javascript_on;
	global $toptions;
	if($toptions['personalbook']+$toptions['globalbook']==0) $local=0;
	else $local=$toptions['nickname']+$toptions['name']+$toptions['email'];
	if($toptions['ldap']==0 || empty($ldap_server) || !extension_loaded('ldap')) $remote=0;
	else $remote=$toptions['ldapnickname']+$toptions['ldapname']+$toptions['ldapemail'];
	if ($local+$remote==0 || $toptions['enable']==0 || !$javascript_on)	return;
	include_once (SM_PATH . 'functions/addressbook.php');

	// Open addressbook without error messages
	$abook = addressbook_init(false, false);
//	if ($abook->localbackend == 0) return;
	// No personal address book is defined

	if($toptions['personalbook']+$toptions['globalbook']==2)	$alist = $abook->list_addr(); //Does not get addresses from LDAP
	else if($toptions['personalbook']==1) $alist = $abook->list_addr(1); 
	else if($toptions['globalbook']==1) $alist = $abook->list_addr(2); 
	if (! is_array($alist)) return ;
	// Some sort of error

	$addrs = array();
	$keys=array();
	$lkeys=array();
	$rkeys=array();
	$keys[]="email";
	$keys[]="firstname";
	$keys[]="lastname";

	if($local==0) {
		$addrs="";
		$ljkeys="[]";
	} else {
		if($toptions['nickname']) $lkeys[]="nickname";
		if($toptions['name']) {
			$lkeys[]="name";
			$lkeys[]="lastname";
		}
		if($toptions['email']) $lkeys[]="email";
		$ljkeys='["' . implode('","',$keys).'","'.implode('","',$lkeys) . '"]';
		$tmpArr=array();
		$sortedArr=array();
		foreach ($alist as $k=>$val) {
			$temp=array();
			foreach($keys as $v) {
				$temp[]=addcslashes(trim(str_replace("\n"," ", $val[$v])),'"');
			}
			foreach($lkeys as $v) {
				$vv=trim(strtolower($val[$v]));
				if(empty($tmpArr[$v][$vv])) $tmpArr[$v][$vv]=array($k);
				else $tmpArr[$v][$vv][]=$k;
				$temp[]=addcslashes(trim(str_replace("\n"," ", $val[$v])),'"');
			}
			$addrs[] = '"' . implode('","',$temp) . '"';
		}
		if (count($addrs) > 0)	$addrs = "[".implode("],\n[", $addrs)."]";
		else $addrs="";
	}
	foreach($tmpArr as $k=>$v) {
		if(ksort($v)) $tmpArr[$k]=$v;
	}
	foreach($tmpArr as $k=>$v) {
		$sortedArr[$k]=array();
		foreach($v as $sk=>$sv) {
			settype($sk,"string");
			if(!empty($sk)) $sortedArr[$k][]=array($sk,$sv);
		}
	}
	if($remote==0) {
		if($addrs=="") return;
		$rjkeys="[]";
	}	else {
		if($toptions['ldapnickname']) $rkeys[]="nickname";
		if($toptions['ldapname']) {
			$rkeys[]="name";
			$rkeys[]="lastname";
		}
		if($toptions['ldapemail']) $rkeys[]="email";
		$rjkeys='["' . implode('","',$keys).'","'.implode('","',$rkeys) . '"]';
	}
	echo "<script type='text/javascript' language='Javascript'><!--\n";
	if ($toptions['insensitive'])	{
		echo "function prep(a) {return a.toLowerCase();}\n";
		echo "function cmp(a,b) {return (a!=undefined && b!=undefined) && (a.toLowerCase() == b);}\n";
	} else	{
		echo "function prep(a) {return a}\n";
		echo "function cmp(a,b) {return (a!=undefined && b!=undefined) && (a == b);}\n";
	}
	echo "//-->\n</script>";
	echo '<script type="text/javascript" src="'.SM_PATH.'plugins/tbaddr/jquery-1.1.4.pack.js"></script>';
?>
<script type="text/javascript" language="Javascript">
<?php echo "<!--"?>

var sortedArr=<?php echo php2js($sortedArr)?>;
var tbLocal=[<?php echo $addrs?>];
var tbarrays=[tbLocal, []];
var tbkeys=[<?php echo $ljkeys.",".$rjkeys?>];
var qkeys=['name','nickname','firstname','lastname','email'];
var addrTypes=["send_to","send_to_cc","send_to_bcc"];
var o=[<?php echo '"'._("To").':","'._("CC").':","'._("BCC").':"'?>];
var tbaddr_local=<?php echo $local?>;
var tbaddr_ldap=<?php echo $remote?>;
var ldapMax=0;
var tbaddr_emptycc=<?php echo $toptions['emptycc']?>;
var tbaddr_emptybcc=<?php echo $toptions['emptybcc']?>;
var minLen=[<?php echo $toptions['minlen'].",".$toptions['ldapminlen']?>];
var throbbers;
var rootDir="<?php echo SM_PATH?>plugins/tbaddr";
if(tbaddr_ldap) throbbers=[rootDir+"/images/notworking.gif", rootDir+"/images/working.gif"];
else throbbers=[rootDir+"/images/spacer.gif",rootDir+"/images/spacer.gif"];


function isArray(obj) {
	if(obj!=undefined && obj.isArray!=undefined) return true;
	else return false;
}

Array.prototype.isArray = function(){return true;}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function array_search(needle, haystack) {
	var n = haystack.length;
	for (var i=0; i<n; i++) {
		if (haystack[i]==needle) return i;
	}
	return -1;
}

function htmlchars(s) {
 s=s.replace(/</g,"&lt;");
	s=s.replace(/>/g,"&gt;");
	return s;
}

function debug(d,clear) {
	if(clear==true) document.compose.body.value="";
	if(isArray(d)) {
		var n = d.length;
		for (var i=0; i<n; i++) {
//			if(isArray(d[i])) debug(d[i]);
			document.compose.body.value+=d[i]+",";
		}
		document.compose.body.value+="\n";
	} else {
		document.compose.body.value+=d+"\n";
	}
}

function tbaddrHL (s,hl,c) {
	var beg=s.substring(0,hl);
	if(c==undefined) return "<span>"+beg+"</span>"+s.substring(hl);
	else {
		if(cmp(beg, c.substring(0, hl))) return "<span>"+beg+"</span>"+s.substring(hl);
		else return s;
	}
}

function tbaddrReformat (email) {
	var tbarray = tbarrays[0];
	if(tbarray.length==0) return email;
	var i;
	email=trim(email);
	for (i = 0; i < tbarray.length; i++) {
		if (tbarray[i][0]!=undefined && tbarray[i][0].toLowerCase()==email.toLowerCase()) break;
	}
	if(i==tbarray.length) return email;
	var name=tbarray[i][1]+" "+tbarray[i][2];
	if(email.indexOf(',')!=-1) return email;
	else	return '"'+name+'" <'+email+'>';
}

function abortQuery () {
	if(ajaxQuery==undefined) return;
	ajaxQuery.abort();
	delete ajaxQuery;
}

function searchLDAP (src) {
	if(ajaxQuery!=undefined) abortQuery();
	var s=src.srch;
	if(s==src.lsrch || src.value=="" || s.length < minLen[1] || (src.lsrch!=undefined && cmp(s.substring(0,src.lsrch.length),src.lsrch) && tbarrays[1].length<ldapMax)) {
		src.TB.gif.setAttribute("src",throbbers[0]);
		return;
	}

	ajaxQuery =  $.ajax({
		beforeSend: function() {
			src.TB.gif.setAttribute("src",throbbers[1]);
		},
		type:       'GET',
		url:        rootDir+'/query.php',
		dataType:   'json',
		data:       's='+escape(s),
		error:						function() {
			src.TB.gif.setAttribute("src",throbbers[0]);
		},
		success:    function (data) {
			if(isArray(data) && ajaxQuery!=undefined) {
				var slen=s.len;
				if(tbarrays[1].length>ldapMax) ldapMax = tbarrays[1].length;
				tbarrays[1]=[];
				for(var j=0;j<data.length;j++) {
					var n = [];
					for(var i=0;i<data[j].length;i++) {
							if(data[j][i]==null) n[qkeys[i]]='';
							else n[qkeys[i]]=data[j][i];
					}
					tbarrays[1][j]=[];
					for(var i=0; i<tbkeys[1].length;i++) {
						tbarrays[1][j][i]=n[tbkeys[1][i]];
					}
				}
				if(data.length>0) tbUpdate(curAddr,false);
				src.lsrch = src.srch;
			}
			src.TB.gif.setAttribute("src",throbbers[0]);
		}
	});

}

var tbArr=null;
var tbSearch='';
function tbaddrRange(beg,end) {
	if(tbArr[beg]==undefined || tbArr[end]==undefined) {
		return false;
	}
	var s=tbArr[beg][0].toLowerCase();
	if(beg==end) {
		if(cmp(s.substring(0, tbSearch.length), tbSearch)) return [beg,end];
		else return false;
	}
	var e=tbArr[end][0].toLowerCase();
	var mid = Math.floor((beg+end)/2);
	var m='';
	if(mid==beg) {
		le=beg;
		rb=end;
		m=s;
	} else {
		le=mid;
		rb=mid;
	 m=tbArr[mid][0].toLowerCase();
	}
	var c=cmp(m.substring(0, tbSearch.length), tbSearch);
	if(c) {
		var l=tbaddrRange(beg,le)
		var r=tbaddrRange(rb,end);
		if(l==false) return r;
		else if(r==false) return l;
		else return [l[0],r[1]];
	} else if(m>tbSearch) {
		return tbaddrRange(beg,le);
	}	else {
		return tbaddrRange(rb,end);
	}
}

var matches = [];
var hmatches = [];
var matched = 0;
function tbaddrFind (src) {
	var s=src.srch;
	if(s=="") return "";
	var slen = s.length;
	var lastMatch = "";
	matched = 0;
	matches = []; 
	hmatches = [];
	for(var k = 0; k < 2; k++) {
		if(slen < minLen[k]) continue;
		var tbkey = tbkeys[k];
		var tbarray=tbarrays[k];
		if(k==0) {
			var range=false;
			for(var key in sortedArr) {
				tbArr=sortedArr[key];
				tbSearch=s.toLowerCase();
				if(tbArr.length>0) {
					range=tbaddrRange(0,tbArr.length-1);
					if(range!==false) {
						var tbArrKeys=[];
						for (var i = range[0]; i <= range[1]; i++) {
							tbArrKeys=tbArr[i][1];
							for (var j = 0; j < tbArrKeys.length; j++) {
								tbaddrMatch(tbarray[tbArrKeys[j]],tbkey,s);
							}
						}
					}
				}
			}
		} else {
			var l = tbarray.length;
			for (var i = 0; i < l; i++) {
				tbaddrMatch(tbarray[i],tbkey,s);
			}
		}
	}
	src.TB.matches=matches;
	src.TB.hmatches=hmatches;
	src.TB.update=true;
	if(!src.keyDown && matched > 0) return s + " >> " + matches[src.TB.offset];
	else return "";
}

function tbaddrMatch(i,tbkey,s) {
	var foundMatch=false;
	var slen = s.length;
	var hslen = htmlchars(s).length;
	var name=trim(i[1]+" "+i[2]);
	var h = [];
	h['email'] = htmlchars(i[0]);
	h['name']=name;
	h['lastname']="";
	h['nickname']="";
	for(var j = 3; j < i.length; j++) {
		if (cmp(i[j].substring(0, slen), s)) {
			foundMatch=true;
			h[tbkey[j]]=tbaddrHL(htmlchars(i[j]),hslen);
		}
	}
	if(foundMatch) {
		var email=i[0];
		if(array_search('"'+name+'" <'+email+'>', matches)==-1) {
			if(h['lastname']!="" && h['name']==name) h['name']=trim(tbaddrHL(htmlchars(i[1]),hslen,s) + " "+h['lastname']);
			if(h['nickname']!="") h['name']+=" ("+h['nickname']+")";
			if(email.indexOf(',')!=-1) {
				matches[matched]=email;
				hmatches[matched++]=h['name'] + " (" + h['email'] + ")";
			} else {
				matches[matched]='"'+name+'" <'+email+'>';
				hmatches[matched++]=h['name'] + " &lt;" + h['email'] + "&gt;";
			}
		}
	}
}

function tbUpdate (src,remote) {
	var newValue;
	var s = src.srch;
	// If we can find something in our lookup list
	if(src.TB.search=="" || !remote) {
		curAddr=src;
		<?php if($remote) echo "if(remote) searchLDAP(src);";?>

		newValue = tbaddrFind(src);
		if (newValue == "")	{
			src.TB.hide();
			return;
		}
	} else {
		src.TB.update=false;
		newValue = s + " >> " + src.TB.matches[src.TB.offset];
	}
	<?php if($toptions['dropdown']) echo "tbDropdown(src);";?>

	if(src._doSelect) {
		var pos = s.length;
		src.value = newValue;
		src._value = src.value;
		if(src.createTextRange) {
			var rNew = src.createTextRange();
			rNew.moveStart("character", pos);
			rNew.select();
		} else if(src.setSelectionRange) {
			src.setSelectionRange(pos,src.value.length);
		}
	}
	src.TB.search = s;
}

function tbaddrCore (src) {
	var s = src.value;
	src.srch = prep(s);
	src._value = s;
	// do not search for nothing or if there are commas
	if (s=="" || s.indexOf(',')!=-1)	{
		abortQuery();
		src.TB.hide();
		src.TB.gif.setAttribute("src",throbbers[0]);
		return;
	}
	tbUpdate(src,true);
}

function getSelection (e) {
	if('selectionStart' in e) {
		var l = e.selectionEnd - e.selectionStart;
		var range = { start: e.selectionStart, end: e.selectionEnd, length: l, text: e.value.substr(e.selectionStart, l) };
		return range;
	} else if(document.selection) {
		e.focus();
		var r = document.selection.createRange();
		if (r === null) return { start: 0, end: 0, length: 0, text: "" }
		var re = e.createTextRange();
		var rc = re.duplicate();
		re.moveToBookmark(r.getBookmark());
		rc.setEndPoint('EndToStart', re);
		var range = { start: rc.text.length, end: rc.text.length + r.text.length, length: r.text.length, text: r.text };
		return range;
	} else return null;
}

function tbaddrKeyUp (e) {
	// If there is no change in the field, ignore
	this.keyDown=false;
	if (this.value == this._value)	return;

	range=getSelection(this);
	var s=range['start'],l=range['end'];
	if(s==l && s < this.value.length) this._doSelect=false;

	if (!this.createTextRange && !this.setSelectionRange)	return;
	if(e == undefined) e=event;
	kc=e.keyCode ? e.keyCode:e.which;
	if(kc == 8) {
		this.TB.offset=-1;
		this._doSelect=false;
	}

	tbaddrCore(this);
}

function tbaddrKeyDown(e) {
	this.keyDown=true;
	if(e == undefined) e=event;
	kc=e.keyCode ? e.keyCode:e.which;
	if(kc == 27) {
		this.TB.hide();
		this.value = this.TB.search;
		this._doSelect=false;
		return e.returnValue=false;
	}
	if(kc != 8) this._doSelect=true;
	if (isNaN(this.TB.offset))	this.TB.offset = 0;
	var shift=false;
	if(e.shiftKey || (e.modifiers & e.SHIFT_MASK)) shift=true;
	if(!shift) {
		range=getSelection(this);
		var s=range['start'],l=range['end'];
		if(kc == 37) {
			if(this.value!=this.TB.search && this.TB.search!='') {
				this.value = this.TB.search;
				if(this.createTextRange) {
					var rNew = this.createTextRange();
					rNew.moveStart("character", s);
					rNew.select();
				}
			}
			this._doSelect=false;
		} else if(kc == 39 || kc == 9 || kc == 13 || kc == 35 || kc == 36) {
			var d=this.value.lastIndexOf(" >> ");
			if(s != l && d != -1 && d >= s && d < l) {
				this.value=this.value.substring(d+4,this.value.length);
				this.TB.hide();
				this.TB.offset = 0;
				this.TB.search = "";
				if(kc==9) operaTabbed=opera;
				newAddrAfter(this);
/*
				if(opera) {return true;}
				else return e.returnValue=false;
//*/
				return e.returnValue=false;
			} else if(kc == 13 && this.value!="") {
				newAddrAfter(this);
				return e.returnValue=false;
			}
		}
	}

	if (kc == 38 || kc == 40 || kc == 33 || kc == 34) {
		if (this.TB.search != "" && this.TB.matches.length>0) {
			var oldoffset=this.TB.offset;
			if (kc == 40) {
						++this.TB.offset;
			} else if (kc == 38) {
						--this.TB.offset;
			} else if (kc == 33) {
						this.TB.offset=this.TB.offset-this.TB.nMaxSize;
			} else if (kc == 34) {
						this.TB.offset=this.TB.offset+this.TB.nMaxSize;
			}
			if(this.TB.offset>=this.TB.matches.length) this.TB.offset=this.TB.matches.length-1;
			if(this.TB.offset<0) this.TB.offset=0;
			if(oldoffset==this.TB.offset) return e.returnValue=false;
			this.value = this.TB.search;
			tbaddrCore(this);
		} else this.TB.offset = 0;
		return e.returnValue=false;
	} else if(kc!=39 && kc!=37) {
		this.TB.offset = 0;
		this.TB.search = "";
	}
}

function remAddr(r) {
	if(tbRows.length==0) return;
	if(r==undefined || r.type!="text") r=this;
	var ecount=0;
	var eindex=-1;
	var index=-1;
	//Look for empties and find which row is being removed
	for(var i=0;i<tbRows.length;i++) {
		if(tbRows[i].TB.tAddr.value=="") {
			ecount++;
			eindex = i;
		}
		if(r.TB==tbRows[i].TB) index=i;
	}
	//If removing the last row, focus on the subject, otherwise, focus on the next one
	if(index==tbRows.length-1) {
		subjectRow.subjectInput.focus();
	} else {
		tbRows[index+1].TB.addrFocus();
	}
	if(ecount > 1 || index!=eindex) {
		r.TB.tRow.parentNode.removeChild(r.TB.tRow);
	} else {
		r.TB.tAddr.value="";
	}
	updateAddrs();
}

function tbaddr(newType, newAddress) {
	// initialize member variables
	this.tRow = document.createElement("tr");
	this.td1 = document.createElement("td");
	this.td1.setAttribute("align", "right");
	this.tSel = document.createElement("select");
	this.tSel.setAttribute("name", "v_type");
	for(var j=0;j<o.length;j++) {
		this.tSel.options[j]=new Option(o[j],addrTypes[j]);
	}
	this.tSel.selectedIndex=newType;
	this.td2 = document.createElement("td");
	this.td2.setAttribute("align", "left");
	try {
		this.tAddr = document.createElement("<input name='tbaddr_to'>");
	} catch (e) {
		this.tAddr = document.createElement("input");
	}
	this.tAddr.name='tbaddr_to';
	this.tAddr.setAttribute("name","tbaddr_to");
	this.tAddr.value = newAddress;
	this.tAddr.setAttribute("size", fieldSize);
	this.tBut = document.createElement("input");
	this.tBut.setAttribute("type","button");
	this.tBut.setAttribute("value","<?php echo _("remove")?>");
	this.nobr = document.createElement("nobr");
	this.aDiv = document.createElement("div");
	this.aDiv.className = "TBADiv";
	if(ie6) {
		this.aFrame = document.createElement("iFrame");
		this.aFrame.src=rootDir+"/images/spacer.gif";
		this.aFrame.setAttribute('src',rootDir+"/images/spacer.gif");
		this.aFrame.setAttribute('scrolling','no');
		this.aFrame.setAttribute('frameborder','0px');
		this.aFrame.className = "TBIFrame"
		this.aFrame.style.visibility='hidden';
		this.aDiv.appendChild(this.aFrame);
		this.aFrame.TB = this;
	}
	this.bDiv = document.createElement("div");
	this.bDiv.className = "TBBDiv";
	this.gif = document.createElement("img");
	this.gif.setAttribute("src",throbbers[0]);
	this.tRow.appendChild(this.td1);
	this.tRow.appendChild(this.td2);
	this.td1.appendChild(this.tSel);
	this.aDiv.appendChild(this.tAddr);
	this.aDiv.appendChild(this.bDiv);
	this.aDiv.appendChild(this.gif);
	this.aDiv.appendChild(this.tBut);
	this.nobr.appendChild(this.aDiv);
	this.td2.appendChild(this.nobr);
	this.offset = 0;
	this.loffset = 0;
	this.matches = [];
	this.nMaxSize = 15;
	this.mousing = false;
	this.search = "";
	this.update=true;

	this.tRow.TB = this;
	this.tSel.TB = this;
	this.tAddr.TB = this;
	this.tBut.TB = this;
	this.bDiv.TB = this;
	this.aDiv.TB = this;
	this.gif.TB = this;
	// attach handlers
	this.tAddr.onfocus = function(){alreadyFocused=true;};
	this.tSel.onchange = updateAddrs;
	this.tAddr.onblur = addrBlur;
	if(tbaddr_local+tbaddr_ldap>0) {
		this.tAddr.setAttribute("autocomplete","off");
		if(opera) this.tAddr.onkeypress = tbaddrKeyDown;
		else this.tAddr.onkeydown = tbaddrKeyDown;
		this.tAddr.onkeyup = tbaddrKeyUp;
		this.tBut.onclick = remAddr;
		this.bDiv.onselectstart = function (e) {if(e==undefined) e=window.event;return e.returnValue=false;};
		this.bDiv.onmouseout = function() {this.TB.mousing=false;if(this.TB.offset>-1) {var s = this.TB.loffset; this.TB.hlDiv=this.childNodes[this.TB.offset-s];if(this.TB.hlDiv!=undefined) this.TB.hlDiv.className = "TBSelected";}};
		this.aDiv.style.zIndex = 0;
	} else {
		this.tBut.style.visibility="hidden";
	}
}

tbaddr.prototype.addrFocus = function() {
	this.tAddr.focus();
}

tbaddr.prototype.hide = function() {
	if(this.bDiv.style.visibility=="visible") {
		this.aDiv.style.zIndex=0;
		this.bDiv.style.visibility = "hidden";
		if(ie6) this.aFrame.style.visibility='hidden';
	}
}

function newAddrAfter(p) {
	newAddr(p.TB.tSel.selectedIndex,"",false,true,p.TB.tRow);
}

function newAddr(newType,newAddress,first,addrFocus,oldRow) {
	if(isNaN(newType)) newType=0;
	if(newAddress==undefined) newAddress="";
	if(first==undefined) first=false;
	if(addrFocus==undefined) addrFocus=true;
	var i,j=-1;
	for(i=0;i<rows.length;i++) {
		if(rows[i].TB != undefined && rows[i].TB.tAddr.value=="") {
			if(addrFocus && j==-1) j=i;
			if((rows[i].TB.tSel.selectedIndex==newType || (newType==0 && oldRow==undefined)) && newAddress=='') {
				if(oldRow!=undefined) j=i;
				if(addrFocus && j!=-1) rows[j].TB.addrFocus();
				return;
			}
		}
		if(rows[i]==subjectRow || (first && rows[i].TB != undefined)) break;
	}
	if(i>0 && rows[i-1].TB != undefined && rows[i-1].TB.tAddr.value=="" && oldRow==undefined) {
		if(addrFocus) {
			if(j!=-1) rows[j].TB.addrFocus(); else rows[i-1].TB.addrFocus();
		}
		if((rows[i-1].TB.tSel.selectedIndex==newType || j!=-1) && newAddress=="" ) {
			return;
		}
	}
	var newtb = new tbaddr(newType,newAddress);
	if(oldRow==undefined) rows[i].parentNode.insertBefore(newtb.tRow,rows[i]);
	else oldRow.parentNode.insertBefore(newtb.tRow,oldRow.nextSibling);
	if(!first && addrFocus) {
		if(operaTabbed) {
			newtb.tSel.focus();
		} else {
			newtb.addrFocus();
		}
		operaTabbed=false;
	}
	if(newAddress!="") updateAddrs();
	return newtb.tAddr;
}

function addrBlur(p) {
	if(p==undefined || p.type!="text") p=this;
	curAddr=null;
	if(ajaxQuery!=undefined) abortQuery();
	p.TB.gif.setAttribute("src",throbbers[0]);
	for(var i=0;i<tbRows.length;i++) {
		if(tbRows[i].TB.mousing==true) {
			if(tbRows[i].TB==p.TB) {
				if(!opera) {
					if(p.createTextRange) {
						p.focus();
						var rNew = p.createTextRange();
						rNew.moveStart("character", p.srch.length);
						rNew.select();
					}
				}
				p.TB.mousing=false;
			}
			return;
		}
	}
	//Clean up autocompleted addresses
	var j=0;
	var d=0;
	var cleaned=[];
	var temp=p.value.split(",");
	for(var i=0;i<temp.length;i++) {
		d=temp[i].lastIndexOf(" >> ");
		if(d != -1) {
			temp[i]=temp[i].substring(d+4);
		}
		if(trim(temp[i])!="") cleaned[j++]=trim(temp[i]);
	}
	p.value=cleaned.join(",");
	updateAddrs();
	p.TB.hide();
	newAddr(0,"",false,false);
}

function updateAddrs() {
	if(tbRows.length==0) return;
	var newVals=[[],[],[]];
	//Collect all entered addresses
	for(var i=0;i<tbRows.length;i++) {
		var nv=tbRows[i].TB.tAddr.value;
		if(nv!="") newVals[tbRows[i].TB.tSel.selectedIndex].push(nv);
	}
	//Place addresses
	for(var i=0;i<addrTypes.length;i++) {
		hiddenAddrs[addrTypes[i]].value=newVals[i].join(",");
	}
}

function tbDropdown(t) {
	var ac = t.TB;
	var txt = ac.tAddr.value;
	var i;

	// count the number of strings that match the text-box value
	var nCount = ac.matches.length;
	var start=ac.loffset;
	
//	if ( (ac.nMaxSize == -1) || (nCount <= ac.nMaxSize && nCount > 0) ) {
	if(ac.offset >= start+ac.nMaxSize) ac.loffset=ac.offset-ac.nMaxSize+1;
	else if(ac.offset < start) ac.loffset=ac.offset;
	else {
		var divs=ac.bDiv.childNodes;
		if(ac.offset > -1 && !ac.update && divs[ac.offset-start]!=undefined) {
			ac.hlDiv.className = "TBNormal";
			divs[ac.offset-start].className = "TBSelected";
			ac.hlDiv=divs[ac.offset-start];
			return;
		}
	}
	if(ac.loffset<0) ac.loffset=0;
	start=ac.loffset;
	if (ac.nMaxSize == -1 || nCount > 0) {
		var maxSize = ac.nMaxSize > nCount ? nCount : ac.nMaxSize;
		// clear the popup-div.
		ac.bDiv.innerHTML='';
//		while(ac.bDiv.hasChildNodes())	ac.bDiv.removeChild(ac.bDiv.firstChild);
			
		// add each string to the popup-div
		for ( i = start; i < maxSize + start; i++ ) {
			var cDiv = document.createElement('div');
			cDiv.innerHTML = ac.hmatches[i];
			cDiv.addr = ac.matches[i];
			if(i==ac.offset) {
				cDiv.className = "TBSelected";
				ac.hlDiv=cDiv;
			} else cDiv.className = "TBNormal";
			cDiv.onmousedown = function(e) {if(e==undefined) e=window.event;this.TB.mousing=true;return e.returnValue=false;};
			cDiv.onmouseup = function() {this.TB.tAddr.value = this.addr;this.TB.hide();this.TB.mousing=false;newAddrAfter(this);};
			cDiv.onmouseover = function() {this.TB.mousing=true && opera;this.className = "TBSelected";if(this.TB.hlDiv!=this && this.TB.hlDiv!=undefined) this.TB.hlDiv.className = "TBNormal";this.TB.hlDiv=this;};
			cDiv.onmouseout = function() {this.TB.mousing=false;this.className = "TBNormal";};
			cDiv.TB = ac;			
			ac.bDiv.appendChild(cDiv);
		}
		ac.aDiv.style.zIndex=1;
		ac.bDiv.style.visibility = "visible";
		if(ac.bDiv.clientWidth<fieldWidth)	{
			ac.bDiv.firstChild.style.width = fieldWidth-2;
		}
		if(ie6) {
			ac.aFrame.style.width=ac.bDiv.clientWidth+2;
			ac.aFrame.style.height=ac.bDiv.clientHeight+2;
			ac.aFrame.style.visibility='visible';
		}
	}	else { // hide the popup-div 
		ac.bDiv.innerHTML = '';
		ac.hide();
	}
}

function initac() {
	//Find the subject line, collect the rows, and get all pre-existing addresses
	try {
		var newsend;
		var x;
		// Need special code for IE6, since it doesn't support z-index on select combo boxes
		if (navigator.appName == 'Microsoft Internet Explorer'	&& ver < 7) ie6=true;
		if (navigator.appName == 'Opera') opera=true;
		var subjectInput=document.getElementsByName('subject')[0];
		subjectRow=subjectInput.parentNode.parentNode;
		subjectRow.subjectInput=subjectInput;
		rows=subjectRow.parentNode.getElementsByTagName('tr');
		var par=rows[0].parentNode;
		var oldVals=[];
		//Collect pre-existing addresses
		for(var i=0;i<addrTypes.length;i++) {
			var tmp = composeForm[addrTypes[i]];
			if(tmp!=undefined) {
				oldVals[i]=tmp.value;
				hiddenAddrs[addrTypes[i]]=tmp;
			} else {
				throw('Could not find ['+addrTypes[i] + '] in compose form.')
			}
		}
		//Hide old inputs
		var newrow = document.createElement("tr");
		var td = document.createElement("td");
		var tbl = document.createElement("table");
		var tbdy = document.createElement("tbody");
		newrow.style.display='none';
		td.colSpan=2;
		for(var i=0;i<addrTypes.length;i++) {
			tbdy.appendChild(hiddenAddrs[addrTypes[i]].parentNode.parentNode);
		}
		tbl.appendChild(tbdy);
		td.appendChild(tbl);
		newrow.appendChild(td);
		par.appendChild(newrow);
	} catch (e) {
		alert('Failed to initialize address completion!\n'+e);
		return;
	}
	var v='';
	var squote=false;
	var dquote=false;
	//Add new rows for pre-existing addresses
	for(var i=0;i<oldVals.length;i++) {
		v=oldVals[i];
		var s=0;
		if(v!="") {
			for(var j=0;j<v.length;j++) {
				if(v[j]=="'" && !dquote) squote=!squote;
				if(v[j]=='"' && !squote) dquote=!dquote;
				if(!dquote && !squote && v[j]==',') {
					newVal=trim(v.substring(s,j));
					newAddr(i,tbaddrReformat(newVal),false,false);
					s=j+1;
				}
			}
			newAddr(i,tbaddrReformat(trim(v.substring(s))),false,false);
		}
	}
	//Create empty addresses rows per user prefs
	newAddr(0,"",false,false);
	if(tbaddr_emptycc==1) newAddr(1,"",false,false);
	if(tbaddr_emptybcc==1) newAddr(2,"",false,false);
}

var opera=false;
var operaTabbed=false;
var ie6=false;
var ver = -1;
var ua = navigator.userAgent;
var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
var ajaxQuery=null;
var curAddr=null;
var composeForm=null;
var fieldSize;
var fieldWidth;
var subjectRow=null;
var rows=[];
var hiddenAddrs={};
var tbRows=document.getElementsByName('tbaddr_to');
$(document).ready(function() { 
	if (re.exec(ua) != null) ver = parseFloat( RegExp.$1 );
	composeForm=document.getElementsByName('compose');
	if(composeForm.length==1) {
		composeForm=composeForm[0];
		fieldSize=composeForm['send_to'].size;
		fieldWidth=composeForm['send_to'].clientWidth+1;
		initac();
	} else {
		alert('Failed to initialize address completion!\nCould not find compose form.');
	}
});
//-->
</script>
<?php
}

function tbaddr_abook_init_do() {
	global $javascript_on,$ldap_server;
	global $toptions;
	if($toptions['personalbook']+$toptions['globalbook']==0) $local=0;
	else $local=$toptions['nickname']+$toptions['name']+$toptions['email'];
	if($toptions['ldap']==0 || empty($ldap_server) || !extension_loaded('ldap')) $remote=0;
	else $remote=$toptions['ldapnickname']+$toptions['ldapname']+$toptions['ldapemail'];
	if ($local+$remote==0 || $toptions['enable']==0 || !$javascript_on)	return;
	sqgetGlobalVar('show' ,   $show);
	sqgetGlobalVar('query',   $query,   SQ_POST);
	sqgetGlobalVar('listall', $listall, SQ_POST);
	if ($show == 'form' && !isset($listall)) return;
	$output = '<script type="text/javascript" language="Javascript">
	<!--
		function to_addressnew(t) {
			parent.opener.newAddr(0,parent.opener.tbaddrReformat(t),true);
		}
		function cc_addressnew(t) {
			parent.opener.newAddr(1,parent.opener.tbaddrReformat(t),true);
		}
		function bcc_addressnew(t) {
			parent.opener.newAddr(2,parent.opener.tbaddrReformat(t),true);
		}
		function rewriteJS() {
			to_address=to_addressnew;

			cc_address=cc_addressnew;

			bcc_address=bcc_addressnew;
		}
		window.onload=rewriteJS;
	//-->
	</script>';
	if (check_sm_version(1,5,2)) {
	 global $oTemplate,$template_output;
/*
		global $squirrelmail_plugin_hooks,$tbaddr_output_location;
		$squirrelmail_plugin_hooks['template_construct_body.tpl']['tbaddr'] = 'tbaddr_output_template';
		$tbaddr_output_location = 'body_after';
		$squirrelmail_plugin_hooks['template_construct_page_header.tpl']['tbaddr'] = 'tbaddr_output_template';
		$tbaddr_output_location = 'page_header_top';
//*/
		$oTemplate->assign('output', $output);
		$template_output = $oTemplate->fetch('plugins/tbaddr/abook.tpl');
	} else {
		echo $output;
	}
}

function tbaddr_output_template() {
	global $tbaddr_output_location, $template_output;
	return array($tbaddr_output_location => $template_output);
}

function tbaddr_loadprefs() {
	global $username,$data_dir;
	global $tbaddr_loptions,$toptions,$ldapnicknames;
	$ldapnicknames=array();
	include(SM_PATH . 'plugins/tbaddr/config.php');
	include_once (SM_PATH . 'functions/addressbook.php');

	//Check for global address book
	$abook = addressbook_init(false, false);
	$abooklist=$abook->get_backend_list();
	$globalBook=false;
	foreach($abooklist as $k=>$v) {
		if($v->sname==_("Global address book")) {
			$globalBook=true;
			break;
		}
	}

	tbaddr_options_init();
	//Load prefs from user
	foreach($tbaddr_loptions as $optType=>$arr) {
		foreach($arr[1] as $k=>$v) {
			$toptions[$k] = getPref($data_dir, $username,'tbaddr_'.$k,${$k.'_autocomplete'});
		}
	}
	$tmp=getPref($data_dir, $username,'tbaddr_local',-1);
	if($tmp!=-1) {
		if(getPref($data_dir, $username,'tbaddr_personalbook',-1)==-1)		$toptions['personalbook']=$tmp;
		if(getPref($data_dir, $username,'tbaddr_globalbook',-1)==-1)		$toptions['globalbook']=$tmp;
	}
	//Remove global address book options if there isn't one
	if(!$globalBook) {
		unset($tbaddr_loptions['localOpts'][1]['globalbook']);
		$toptions['globalbook']=0;
	}
}

function tbaddr_options_init() {
	global $tbaddr_loptions;
	if (function_exists('nsm_bindtextdomain')) {
		nsm_bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
		nsm_textdomain('tbaddr');
	} else {
		bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
		textdomain('tbaddr');
	}
	$tbaddr_loptions=array(
		'generalOpts'  =>array(_("General Options"),array(
			'enable'       =>array(_("Enable address autocompletion"),1,'generalOpts'),
			'insensitive'  =>array(_("Use case-insensitive searches"),0),
			'dropdown'     =>array(_("Dropdown possible matches"),1,'dropdownOpts'),
			'emptycc'      =>array(_("Start with an empty CC address field"),0),
			'emptybcc'     =>array(_("Start with an empty BCC address field"),0),
		)),
		'dropdownOpts' =>array(_("Dropdown Color Options"),array(
			'textClr'						=>array(_("Text"),2),
			'mtextClr'					=>array(_("Matched Text"),2),
			'bgClr'        =>array(_("Background"),2),
			'selAddrClr'   =>array(_("Selected address"),2),
		)),
		'localOpts'    =>array(_("Local Options"),array(
			'personalbook' =>array(_("Use personal address book"),1,'localOpts'),
			'globalbook'   =>array(_("Use global address book"),1,'localOpts'),
			'name'         =>array(_("Autocomplete on name"),0),
			'nickname'     =>array(_("Autocomplete on nickname"),0),
			'email'        =>array(_("Autocomplete on e-mail address"),0),
			'minlen'       =>array(_("Minimum characters for autocompletion"),3),
		)),
		'ldapOpts'     =>array(_("LDAP Options"),array(
			'ldap'         =>array(_("Use LDAP address book"),1,'ldapOpts'),
			'ldapname'     =>array(_("Autocomplete on name"),0),
			'ldapnickname' =>array(_("Autocomplete on nickname"),0),
			'ldapemail'    =>array(_("Autocomplete on e-mail address"),0),
			'ldapminlen'   =>array(_("Minimum characters for autocompletion"),3),
		))
	);
	if (function_exists('nsm_bindtextdomain')) {
		nsm_bindtextdomain('nasmail',SM_PATH . 'locale');
		nsm_textdomain('nasmail');
	} else {
		bindtextdomain('squirrelmail',SM_PATH . 'locale');
		textdomain('squirrelmail');
	}
}

function tbaddr_options() {
	global $tbaddr_loptions,$toptions,$ldap_server;
	global $color;
	$info = tbaddr_info();


	if (function_exists('nsm_bindtextdomain')) {
		nsm_page_title();
		nsm_bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
		nsm_textdomain('tbaddr');
	} else {
		bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
		textdomain('tbaddr');
	}
	displayPageHeader($color, '');
	echo "<table align='center' width='95%' border='0' cellpadding='1' cellspacing='1' bgcolor='".$color[0]."'>\n";
	echo "<tr><td align='center'><b>"._("Options")." - "._("Address Autocompletion")."</b></td></tr>\n";
	echo "<tr><td align='center' bgcolor='".$color[4]."'>";
 echo '<form name="f" method="post" action="options.php">';
	echo "<table border='0' cellpadding='8' cellspacing='0'><tr><td align='center'>";
	echo _("Select options to control address completion.")."</td></tr>\n";
	echo "<tr><td><table><tr>";
	$count=0;
	$prev=false;
	foreach($tbaddr_loptions as $optType=>$arr) {
		if($optType==_("ldapOpts") && empty($ldap_server)) break;
		echo "<td valign='top' style='border: 1px solid ".$color[0].";'><table id='$optType'>";
		echo "<tr align='center'><td nowrap><b>&nbsp;&nbsp;".$arr[0]."&nbsp;&nbsp;</b></td></tr>";
		foreach($arr[1] as $k=>$v) {
			echo "<tr align='left'><td>";
			if($v[1]==0) {
				echo "<input type='checkbox' name='tbaddr_".$k."_i' id='tbaddr_".$k."_i'";
				if ($toptions[$k]) echo " CHECKED";
				echo "> <label for='tbaddr_{$k}_i' style='cursor: hand; cursor: pointer;' onmouseout='this.style.textDecoration=\"none\";' onmouseover='this.style.textDecoration=\"underline\";'>".$v[0]."</label>";
			} else if($v[1]==1) {
				echo "<input type='checkbox' name='tbaddr_".$k."_i' id='tbaddr_".$k."_i' onclick='toggleOptions(\"".$v[2]."\")'";
				if ($toptions[$k]) echo " CHECKED";
				echo "> <label for='tbaddr_{$k}_i' style='cursor: hand; cursor: pointer;' onmouseout='this.style.textDecoration=\"none\";' onmouseover='this.style.textDecoration=\"underline\";'>".$v[0]."</label>";
			} else if($v[1]==2) {
				echo "<input onclick='colorPicker(this,event);' autocomplete='off' style='font-family: courier new; font-size: 12;' size=7 name='tbaddr_".$k."_i' id='tbaddr_".$k."_i' value='".$toptions[$k]."' /> ".$v[0];
				$prev=true;
			} else if($v[1]==3) {
				echo "<input style='font-family: courier new; font-size: 12;' size=2 name='tbaddr_".$k."_i' id='tbaddr_".$k."_i' value='".$toptions[$k]."' /> ".$v[0];
			}
			echo "</td></tr>\n";
		}
		if($prev) {
			echo "<tr><td align='center'><input type='button' value='"._('Preview')."' onclick='previewDropdown(this,event);'/></td></tr>\n";
			$prev=false;
		}
		echo "</table></td>";
	}
	echo '</tr></table></td></tr>';
	echo "<tr><td align='center'>";
	echo '<input type="submit" value="'._("Save").'" name="submit" />';
	echo "</td></tr>\n";
	echo "</table></form></td></tr>";
	echo "<tr><td align='center'>{$info['english_name']} {$info['version']}";
	echo "</td></tr></table>\n";
	echo '<script type="text/javascript" src="'.SM_PATH.'plugins/tbaddr/colorpicker.js"></script>';
	?>
	<div id='cpickerFillerDiv' style="position:absolute;height:0px;width:462px;visibility:hidden;">Filler div</div>
	<div id='previewDiv' style="border:1px solid black;position:absolute;display:none;"></div>
	<div id='colorPicker' style="position:absolute;height:260px;width:462px; border:1px solid black;background-color:<?php echo $color[0]?>;display:none;">
  <div id="gradientBox" style="cursor:crosshair;top:1px; position:absolute;left:1px;width:256px;height:256px;">
    <img alt='' id="gradientImg" style="display:block;width:256px;height:256px;" src="<?php echo SM_PATH?>plugins/tbaddr/images/color_picker_gradient.png" />
    <img alt='' id="circle" style="position:absolute;height:11px;width:11px;" src="<?php echo SM_PATH?>plugins/tbaddr/images/color_picker_circle.gif" />
  </div>
  <div id="hueBarDiv" style="position:absolute;left:265px; width:35px;height:256px;top:1px;">
    <img alt='' id="hueBarImg" style="position:absolute;height:256px; width:19px;left:8px;" src="<?php echo SM_PATH?>plugins/tbaddr/images/color_picker_bar.png" />
    <img alt='' id="arrows" style="position:absolute; height:9px;width:35px;left:0px;" src="<?php echo SM_PATH?>plugins/tbaddr/images/color_picker_arrows.gif" />
  </div>
  <div style="position:absolute;left:315px;width:144px; height:256px;top:1px;">
  <div style="position:absolute;top:0px;left:0px;">
    <div id="quickColor" style="position:absolute; border-width: 1px 0px 1px 1px; border-style:solid; border-color: black;height:50px;width:72px;top:0px;left:0px;"></div>
    <div id="staticColor" style="position:absolute; border-width: 1px 1px 1px 0px; border-style:solid; border-color: black;height:50px;width:72px;top:0px;left:72px;"></div>
  </div>
  <br />
  <table class='cptbl' width="100%" style="position:absolute;top:55px;">
    <tr>
      <td>Hex: </td>
      <td>
        <input class='ff' size="8" type="text" id="hexBox" onchange="hexBoxChanged();" />
      </td>
    </tr>
    <tr>
      <td>Red: </td>
      <td>
        <input class='ff' size="8" type="text" id="redBox" onchange="redBoxChanged();" />
      </td>
    </tr>
    <tr>
      <td>Green: </td>
      <td>
        <input class='ff' size="8" type="text" id="greenBox" onchange="greenBoxChanged();" />
      </td>
    </tr>
    <tr>
      <td>Blue: </td>
      <td>
        <input class='ff' size="8" type="text" id="blueBox" onchange="blueBoxChanged();" />
      </td>
    </tr>
    <tr>
      <td>Hue: </td>
      <td>
        <input class='ff' size="8" type="text" id="hueBox" onchange="hueBoxChanged();" />
      </td>
    </tr>
    <tr>
      <td>Saturation: </td>
      <td>
        <input class='ff' size="8" type="text" id="saturationBox" onchange="saturationBoxChanged();" />
      </td>
    </tr>
    <tr>
      <td>Value: </td>
      <td>
        <input class='ff' size="8" type="text" id="valueBox" onchange="valueBoxChanged();" />
      </td>
    </tr>
    <tr>
      <td colspan=2>
							<table width='100%'><tr>
								<td align='middle'>
									<input type='button' onclick='closePicker(true)' value='<?php echo _('Save');?>'/>
								</td>
								<td align='middle'>
									<input type='button' onclick='closePicker(false)' value='<?php echo _('Cancel');?>'/>
								</td>
							</tr></table>
						</td>
    </tr>
  </table>
  </div>
	</div>
	<script type="text/javascript">
		function toggleOptions(optType) {
			var inputs;
			var darken=false;
			var tmp=document.getElementById('tbaddr_enable_i');
			//Determine which inputs are 'disabled'
			if(optType=='generalOpts') {
				inputs=document.getElementsByTagName('input');
				darken=!tmp.checked;
			} else {
				var tmptbl = document.getElementById(optType);
				if(tmptbl) inputs=tmptbl.getElementsByTagName('input');
				else return;
				if(optType=='dropdownOpts') {
					darken=!(document.getElementById('tbaddr_dropdown_i').checked);
					tmptbl.parentNode.style.display=darken ? 'none':'';
					return;
				} else {
					if(!tmp.checked) return;
					darken=true;
					for(var i=0;i<inputs.length;i++) {
						if(inputs[i].onclick!=undefined && inputs[i].checked) darken=false;
					}
				}
			}
			//Darken text for options that are 'disabled'
			for(var i=0;i<inputs.length;i++) {
				if(inputs[i].type=='button') inputs[i].disabled=darken;
				else if(inputs[i]!=tmp && inputs[i].id!='' && (optType=='generalOpts' || inputs[i].onclick==undefined)) inputs[i].parentNode.style.color=darken ? dimmedColor:'';
			}

			if(optType=='generalOpts' && !darken) {
				toggleOptions('dropdownOpts');
				toggleOptions('localOpts');
				toggleOptions('ldapOpts');
			}

		}

		function findabsPos(obj) {
			var curleft = curtop = 0;
			if (obj.offsetParent) {
				curleft = obj.offsetLeft
				curtop = obj.offsetTop
				while (obj = obj.offsetParent) {
					curleft += obj.offsetLeft
					curtop += obj.offsetTop
				}
			}

			return [parseInt(curleft), parseInt(curtop)];
		}

		function windowSize() {
			if (document.all) {
				height = document.body.offsetHeight;
				width = document.body.offsetWidth;
			} else {// handles non IE
				height = window.innerHeight;
				width = window.innerWidth;
			}
			return [parseInt(width),parseInt(height)];
		}

		function colorPicker(t,e) {
			if(cPicker.ctl==t && cPicker.style.display!='none') return; 
			var pos = findabsPos(t);
			cPicker.hex.value=t.value;
			hexCh();
			document.getElementById("staticColor").style.backgroundColor = curCol.HexString();
			cPicker.style.left = (pos[0])+'px';
			cPicker.style.top = (pos[1]+t.clientHeight+2)+'px';
			cPicker.style.display='';
			cPicker.ctl=t;
			cancelEvent(e);
			blurPreview();
			hookEvent(document,'click',blurPicker);
		}

		function closePicker(s) {
			if(s==true) cPicker.ctl.value=cPicker.hex.value;
			cPicker.style.display='none';
			unhookEvent(document,'click',blurPicker);
		}

		function blurPicker(e) {
			if(cPicker.style.display=='none' || cPicker.hovering) return;
			closePicker(false);
		}

		function hover() {
			cPicker.hovering=true;
		};

		function unhover() {
			cPicker.hovering=false;
		};

		function previewDropdown(t,e) {
			if(previewDiv.style.display!='none') return;
			t=t.parentNode;
			var pos = findabsPos(t);
			previewDiv.style.display='';
			previewDiv.style.left = (pos[0]+(t.clientWidth-previewDiv.clientWidth)/2)+'px';
			previewDiv.style.top = (pos[1]+t.clientHeight)+'px';
			
			previewDiv.style.color=document.getElementById('tbaddr_textClr_i').value;
			previewDiv.style.backgroundColor=document.getElementById('tbaddr_bgClr_i').value;
			previewDiv.selectedRow.style.backgroundColor=document.getElementById('tbaddr_selAddrClr_i').value;
			for(var f in previewDiv.matchedText) {
				previewDiv.matchedText[f].color=document.getElementById('tbaddr_mtextClr_i').value;
			}
			cancelEvent(e);
			hookEvent(document,'click',blurPreview);
		}

		function blurPreview(e) {
			if(previewDiv.style.display=='none') return;
			previewDiv.style.display='none';
			unhookEvent(document,'click',blurPicker);
		}

		fixGradientImg();
		var curCol = Colors.ColorFromRGB(0,0,0);
		new dragObject("arrows", "hueBarDiv", arrowsLowBounds, arrowsUpBounds, arrowsDown, arrowsMoved, endMovement);
		new dragObject("circle", "gradientBox", circleLowBounds, circleUpBounds, circleDown, circleMoved, endMovement);
		colCh('box');
		var cPicker = document.getElementById('colorPicker');
		cPicker.hovering=false;
		var cFillerDiv = document.getElementById('cpickerFillerDiv');
		if(cFillerDiv) {
			var cLastInput=document.getElementById('tbaddr_selAddrClr_i');
			var cLastInputPos=findabsPos(cLastInput);
			cFillerDiv.style.left=cLastInputPos[0]+'px';
			cFillerDiv.style.top=(cLastInputPos[1]+cLastInput.clientHeight+10)+'px';
			cFillerDiv.style.height=cPicker.style.height;
		}
		var previewDiv = document.getElementById('previewDiv');
		if(previewDiv) {
			var div=document.createElement('div');
			var b=document.createElement('b');
			var f=document.createElement('font');
			f.appendChild(document.createTextNode('Match'));

			div.style.padding='2px';
			b.appendChild(f.cloneNode(true));
			b.appendChild(document.createTextNode('ew Smith <msmith@email.com>'));
			div.appendChild(b);
			previewDiv.appendChild(div);

			div=document.createElement('div');
			b=document.createElement('b');
			div.style.padding='2px';
			b.appendChild(document.createTextNode('Selected '));
			b.appendChild(f.cloneNode(true));
			b.appendChild(document.createTextNode(' <selectedm@email.com>'));
			div.appendChild(b);
			previewDiv.appendChild(div);
			previewDiv.selectedRow=div;

			div=document.createElement('div');
			b=document.createElement('b');
			div.style.padding='2px';
			b.appendChild(document.createTextNode('Pyro ('));
			b.appendChild(f.cloneNode(true));
			b.appendChild(document.createTextNode('es) <'));
			b.appendChild(f.cloneNode(true));
			b.appendChild(document.createTextNode('es451@email.com>'));
			div.appendChild(b);
			previewDiv.appendChild(div);

			previewDiv.matchedText=previewDiv.getElementsByTagName('font');
		}
		//<-Trap bug with IE receiving onmouseout event when image appears underneath pointer
	 document.getElementById("gradientImg").onmouseup = hover;
	 document.getElementById("circle").onmouseup = hover;
	 document.getElementById("hueBarImg").onmouseup = hover;
	 document.getElementById("arrows").onmouseup = hover;
		//<-
		cPicker.onmouseover=hover;
		cPicker.onmouseout=unhover;
		cPicker.hex = document.getElementById('hexBox');
		var dimmedColor='<?php echo $color[4]?>';
		dimmedColor=(parseInt(dimmedColor.replace(/[^(\d|A-F|a-f)]/g,''),16));
		var r = dimmedColor>>16;
		var g = (dimmedColor>>8) & 0xFF;
		var b = dimmedColor & 0xFF;
		if(r<128) r+=48;
		else r-=48;
		if(g<128) g+=48;
		else g-=48;
		if(b<128) b+=48;
		else b-=48;
		dimmedColor=(r<<16) + (g<<8) + b;
		dimmedColor = "#"+dimmedColor.toString(16);
		toggleOptions('generalOpts');
	</script>
	<?php

	if (function_exists('nsm_bindtextdomain')) {
		nsm_bindtextdomain('nasmail',SM_PATH . 'locale');
		nsm_textdomain('nasmail');
	} else {
		bindtextdomain('squirrelmail',SM_PATH . 'locale');
		textdomain('squirrelmail');
	}
}

function tbaddr_save() {
	global $username,$data_dir;
	global $tbaddr_loptions, $toptions;
	$temp='';
	foreach($tbaddr_loptions as $optType=>$arr) {
		foreach($arr[1] as $k=>$v) {
			if($v[1]<=1) setPref($data_dir, $username, 'tbaddr_'.$k, sqgetGlobalVar('tbaddr_'.$k.'_i',$temp) ? 1:0);
			else {
				sqgetGlobalVar('tbaddr_'.$k.'_i',$temp);
				setPref($data_dir, $username, 'tbaddr_'.$k, $temp);
			}
		}
	}
}