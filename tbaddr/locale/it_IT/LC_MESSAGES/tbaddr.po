# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: TBAddr\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-05-11 12:56-0700\n"
"PO-Revision-Date: 2008-03-25 14:30-0800\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Italian\n"

msgid "To"
msgstr "A"

msgid "CC"
msgstr "Cc"

msgid "BCC"
msgstr "BCC"

msgid "General Options"
msgstr "Opzioni Generali"

msgid "Enable plugin"
msgstr "Permettalo"

msgid "Enable address autocompletion"
msgstr "Attivarelo"

msgid "Use case-insensitive searches"
msgstr "Ricerca non sensibile alla rottura"

msgid "Dropdown possible matches"
msgstr "Mostri i risultati possibili"

msgid "Start with an empty CC address field"
msgstr "Inserire un campo CC vuoto"

msgid "Start with an empty BCC address field"
msgstr "Inserire un campo BCC vuoto"

msgid "Local Options"
msgstr "Opzioni locali"

msgid "Use personal address book"
msgstr "Rubrica personale"

msgid "Use global address book"
msgstr "Rubrica globale"

msgid "Use LDAP address book"
msgstr "Rubrica LDAP"

msgid "Preview"
msgstr "Verificare"

msgid "Cancel"
msgstr "Annullare"

msgid "Autocomplete on name"
msgstr "Completare a partire dal nome"

msgid "Autocomplete on nickname"
msgstr "Completare a partire dal soprannome"

msgid "Autocomplete on e-mail address"
msgstr "Completare a partire dall'indirizzo elettronico"

msgid "LDAP Options"
msgstr "Opzioni LDAP"

msgid "Options"
msgstr "Opzioni"

msgid "Address Autocompletion"
msgstr "Auto-completamento di indirizzi elettronici"

msgid "Select options to control address completion."
msgstr "Selezioni le opzioni a completare di indirizzamento."

msgid "Save"
msgstr "Salva"

msgid "Address Autocompletion Options"
msgstr "Opzioni d'Indirizzo Autocompletion"

msgid "Here you may set up your preferences for address autocompletion."
msgstr "Qui, potete configurare l'auto-completamento di indirizzi elettronici."

msgid "Dropdown Color Options"
msgstr "Opzioni di colore"

msgid "Text"
msgstr "Testo"

msgid "Matched Text"
msgstr "Testo corrispondente"

msgid "Background"
msgstr "Colore di fondo"

msgid "Selected Address"
msgstr "Indirizzo scelto"

msgid "Minimum characters for autocompletion"
msgstr "Caratteri minimi prima di completare"

