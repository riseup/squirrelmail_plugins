��    !      $  /   ,      �     �                >     S     l  
   p     {     ~     �     �     �     �     �  @   �     3     @     N  %   [     �     �     �  -   �     �  %   �  $   �           %     (     >     \     t     �  +   �  "   �  /   �     .  #   L     p     t     �  	   �     �     �  
   �  
   �     �  F   �     .     ;     J  $   _     �  
   �     �  4   �     �     �     �     	      	     "	  "   /	     R	     b	                                        	                         !                               
                                                               Address Autocompletion Address Autocompletion Options Autocomplete on e-mail address Autocomplete on name Autocomplete on nickname BCC Background CC Cancel Dropdown Color Options Dropdown possible matches Enable address autocompletion Enable plugin General Options Here you may set up your preferences for address autocompletion. LDAP Options Local Options Matched Text Minimum characters for autocompletion Options Preview Save Select options to control address completion. Selected Address Start with an empty BCC address field Start with an empty CC address field Text To Use LDAP address book Use case-insensitive searches Use global address book Use personal address book Project-Id-Version: TBAddr
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-05-11 12:56-0700
PO-Revision-Date: 2008-03-25 14:30-0800
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-15
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Italian
 Auto-completamento di indirizzi elettronici Opzioni d'Indirizzo Autocompletion Completare a partire dall'indirizzo elettronico Completare a partire dal nome Completare a partire dal soprannome BCC Colore di fondo Cc Annullare Opzioni di colore Mostri i risultati possibili Attivarelo Permettalo Opzioni Generali Qui, potete configurare l'auto-completamento di indirizzi elettronici. Opzioni LDAP Opzioni locali Testo corrispondente Caratteri minimi prima di completare Opzioni Verificare Salva Selezioni le opzioni a completare di indirizzamento. Indirizzo scelto Inserire un campo BCC vuoto Inserire un campo CC vuoto Testo A Rubrica LDAP Ricerca non sensibile alla rottura Rubrica globale Rubrica personale 