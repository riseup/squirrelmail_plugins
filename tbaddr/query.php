<?php
if(file_exists('../../include/init.php')) require('../../include/init.php');
else {
	if(!defined("SM_PATH")) define("SM_PATH",'../../');
	require_once(SM_PATH . 'include/validate.php');
}
include_once (SM_PATH . 'functions/addressbook.php');
include_once (SM_PATH . 'functions/abook_ldap_server.php');
include_once(SM_PATH . 'plugins/tbaddr/functions.php');
$finished=false;
function tbencode($a) {
// return json_encode($a);
	$tmp=array();
	foreach($a as $v) {
		if(!empty($v)) $tmp[] = '["'.implode('","',$v).'"]';
		else $tmp[] = "[]";
	}
	return '['.implode(',',$tmp).']';
}
function terminate() {
	global $finished,$json;
	if(!$finished) {
		echo tbencode($json);
	}
	$finished=true;
	exit(0);
}
function getEntry($abook, $r, $k, $i=0) {
	if($k==='mail') {
		$ret = $r[$k][$i];
	} else if($k==='dn') {
		$ret = $abook->charset_decode($r[$k]);
		$ret = substr(strstr($ret,"uid="),4,strpos(strstr($ret,"uid="),",")-4);
	} else if(empty($r[$k][$i])) $ret = '';
	else $ret = $abook->charset_decode($r[$k][$i]);
	return htmlspecialchars($ret);
}
function validField($a,$k) { //check for characters allowed in LDAP field
	if(!isset($a) || !is_array($a) || !isset($a[$k])) return false;
	return (preg_match('/^([a-z]|[A-Z])([A-Z]|[a-z]|[0-9]|-)\*$/',$a[$k])===1);
}
function validEmail($e) { //check for characters allowed in RFC2822
	return (preg_match('/^([!-\']|[\\*-9]|[\\?-Z]|[_-~]|=)*$/',$e)===1);
}
register_shutdown_function('terminate');
if(!isset($_GET['s'])) terminate();
else $srch=$_GET['s'];
tbaddr_loadprefs();
if(!validEmail($srch)) $toptions['ldapemail']=false;

if (function_exists('set_my_charset')) set_my_charset();

header('Cache-Control: no-cache');
header('Pragma: no-cache');
$attributes = array('givenname' , 'sn', 'mail', 'dn', 'cn');
$json=array();
foreach($ldap_server as $server=>$parms) {
	$abook = new abook_ldap_server($parms);

	$tsrch=$abook->charset_encode($srch);
	$tsrch=str_replace(array('\\','*','(',')','\x00'), array('\5c','\2a','\28','\29','\00'), $tsrch);
	
	if(!validField($ldapnicknames,$server)) $toptions['ldapnickname']=false;
	else {
		$nickfield=strtolower($ldapnicknames[$server]);
		$attributes[]=$nickfield;
	}

	$filter="";
	if($toptions['ldapnickname']) $filter.="($nickfield=$tsrch*)";
	if($toptions['ldapemail']) $filter.="(mail=$tsrch*)";
	if($toptions['ldapname']) {
		if(strpos($tsrch," ")==false) {
			$filter.="(cn=$tsrch*)(sn=$tsrch*)";
		} else {
			$ln=substr($tsrch,strpos($tsrch," ")+1);
			$tsrch=substr($tsrch,0,strpos($tsrch," "));
			$f="(cn=$tsrch*)";
			if($ln!="") {
				$f="(&$f(sn=$ln*))";
			}
			$filter.=$f;
		}
	}
	$filter="(|".$filter.")";

	$maxrows=($abook->maxrows==0 || $abook->maxrows > 50) ? 50:$abook->maxrows; //Do not allow on the fly queries that are greater than 50
	$timeout=($abook->timeout==0 || $abook->timeout > 30) ? 30:$abook->timeout; //Do not allow on the fly queries to last longer than 30 seconds
	@set_time_limit($timeout+5);
	if(!$abook->open() || !($lsearch = @ldap_search($abook->linkid, $abook->basedn, $filter, $attributes, 0, $maxrows, $timeout))) continue;

	$results = @ldap_get_entries($abook->linkid, $lsearch);
	for($i = 0, $count = 0; $i < $results['count'] && $count < $maxrows; $i++) {
		$row = $results[$i];
		if(!empty($row['mail']['count'])) {
			$tmp=array();
			$firstname = getEntry($abook,$row,'givenname');
			$lastname = getEntry($abook,$row,'sn');
			$cn =  getEntry($abook,$row,'cn');
			// if givenName is not defined in this ldap directory, derive it from 'cn' attribute.
			if(empty($firstname) && strlen($cn) > strlen($lastname)) {
				$firstname = trim(str_replace($lastname, '', $cn));
			}
			if($toptions['ldapnickname']) $nickname = getEntry($abook,$row,$nickfield);
			else $nickname="";
			for($j = 0 ; $j < $row['mail']['count'] && $count < $maxrows; $j++, $count++) {
				$tmp[] = trim("$firstname $lastname");
				$tmp[] = $nickname;
				$tmp[] = $firstname;
				$tmp[] = $lastname;
				$tmp[] = getEntry($abook,$row,'mail',$j);
				$json[] = $tmp;
			}
		}
	}
	ldap_free_result($lsearch);
}
echo tbencode($json);
$finished=true;