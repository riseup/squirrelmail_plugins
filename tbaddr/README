Thunderbird Style Address Completion -- Version 0.92

Description
===========

This modifies SquirrelMail to use an address completion that is modeled after
the Thunderbird email client.

Features
========

* Completes on aliases, first and last name, and email address.
* Can scroll up and down through possible matches.
* Shows a dropdown of possible matches.
* Separate fields for each completed address (like Thunderbird).
* Asynchronous LDAP address lookup

Tested on: IE 6/7/8, Firefox 3.5, Opera 9.26, Safari 3.1 (Windows)
Third party tested Konqueror 3.5.6 on FreeBSD 6.2 (Freddie Cash)
Third party tested Safari (Mac) (Daniel Henninger)

Installation
============

As with other plugins, just uncompress the archive in the plugins directory,
go back to the main directory, run configure and add the plugin.  Remember,
for 1.5.2, you must copy the plugin hooks from setup.php to
../config/plugin_hooks.php (or run conf.pl)

To enable LDAP nicknames, enter the nickname field(s) for your LDAP server(s)
in TBAddr config.php.  These must match the index for associated LDAP server
in the SM config.php.  For example, if SM config contains:

$ldap_server[0] = Array(
	'host' => 'memberdir.netscape.com',
	'name' => 'Netcenter Member Directory',
	'base' => 'ou=member_directory,o=netcenter.com'
); 

TBAddr config may contain:

$ldapnicknames[0] = "uid";

Changes
=======
0.92 - Fixed bug involving address book fields that were pure numbers
0.91 - Fixed bug where addresses would be deleted if you hit "left" after clicking on a previously completed address
0.90 - Updated options page to dim inactive choices
     - Local options now specifies personal and global address books separately (when applicable)
     - Javascript code updated
     - Replaced setup.php short tags
0.89 - Fixed Javascript addressbook bug when only ldap completion was on
     - Fixed bug with missing addresses (ie, only one person with a shared last name showing up, introduced in 0.86)
     - Added preview of dropdown to options page
0.88 - Fixed Javascript bug when there are no local addresses
     - HTML compliance improved
0.87 - Fixed bug where plugin overrides focus preference for replies
0.86 - Fixed locale problem on options page
     - LDAP options removed when no LDAP servers are present in SM config.php
     - Fixed IE6 iframe bug on SSL sites (Thanks to Pal)
     - Added Norwegian (Bokmal) translation (Thanks to Pal)
     - Improved performance issues in IE6 with large local addressbooks
     - New French translation (Thanks to Ben)
0.85 - Fixed compatibility problem with abook_group v0.51.1
0.84 - Improved LDAP performance issues, prevent LDAP queries from dropping typed characters
0.83 - Various minor fixes
     - Incorporated suggestions of Alexandros Vellis 
       (minimum length for autocompletion, LDAP improvement)
0.82 - Fixed LDAP searching (syntax error in query.php 0.81)
0.81 - Fixed 'enable' bug
     - Fixed ldap field verification
0.80 - Fixed bug where plugin failed when LDAP was not enabled (introduced in 0.79)
     - Removed throbber if LDAP is not enabled
0.79 - Now using jquery for AJAX (removed sarissa)
     - Fixed bug where first "To" address was being ignored by javascript addressbook
     - Fixed "bug" where multiple addresses were not supported for one addressbook entry
     - Fixed bug where addresses were not loaded when TBAddr loaded after Quicksave plugin
     - Fixed bug with empty addressbook causing plugin to fail to load on reply
     - Better handling of pre-loaded addresses
     - Added color preferences to user options
     - New throbber
0.78 - Fixed bug with LDAP results causing input field to revert to previous state
0.77 - No longer rearranges 'Priority' select in IE6
     - Fixed bug in IE where dropdown fails to close
     - Opera 9.21 supported (fixes for several Opera specific bugs)
0.76 - Fixed bug with LDAP empty result fields causing 'undefined' in completed address
     - Fixed bug with 'undefined' completion when pressing up or down before typing
0.75 - Changes for non-standard characters
     - Fix for LDAP only searches
     - Updated options page
0.74 - Various bug fixes
0.73 - Fixed LDAP email matching
     - Support html characters in results
     - Separate options for LDAP and local addressbooks
     - Support custom nickname field for LDAP server
0.721 - Cleans up old AJAX queries
0.72 - Changed behavior of address fields when typing commas
     - Names containing commas no longer splits across fields
     - Rewrote query.php
     - Improved compatibility with html addressbook and reply to all
0.71 - Improved localization support
     - Included Spanish, French, and Italian plugin translations (feel free to correct any errors)
     - Dropdown no longer shows duplicate addresses when combining locations
0.70 - Implemented asynchronous LDAP address lookup using Sarissa
     - Improved address completion behavior
     - SquirrelMail 1.5.2 no longer supports javascript addressbook
     - Various bug fixes
0.63 - Fixed compose in new window bug for SquirrelMail 1.5.2
     - Updated to support localization (will require updated language files)
0.62 - Preliminary support for SquirrelMail 1.5.2 (tested against 20070502 snapshot)
     - Fixed alignment issue in SquirrelMail 1.5.2
0.61 - Made empty CC and BCC fields optional
     - Updated version to only require SM 1.4.0 (per Tomas Kuliavas)
0.60 - Dropdown now scrolls through all possible matches
     - Previously, dropdown would only appear if there were few enough matches to fit
0.59 - Disabled insertion of javascript when javascript disabled
     - Updated version to only require SM 1.4.5 (per Tomas Kuliavas)
0.58 - Empty address fields are created for To, CC, BCC after autocompletion
0.57 - No longer using PHP short tags
     - Global defaults now accessible through config.php
     - Changed dropdown to highlight all matches
     - Simplified tabbing behavior
     - Initial page contains 3 address lines (To, CC, BCC) to preserve original SquirrelMail appearance
     - No longer removes empty address lines
     - Experimental LDAP support
0.56 - Fixed address book bug (introduced in 0.55)
     - Address book inserts full name plus email
     - Changed options title to "Address Autocompletion Options"
0.55 - Fixed incompatibility with button locations other than "between"
     - added preference to disable autocompletion
     - moved options to dedicated page
0.50 - Initial Release, a small portion of the code ripped from autocomplete, but
mostly discarded.

License
=======

http://opensource.org/licenses/gpl-license.php GNU Public License
