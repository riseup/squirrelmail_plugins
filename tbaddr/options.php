<?php


if(file_exists('../../include/init.php')) require('../../include/init.php');
else {
	if(!defined('SM_PATH')) define('SM_PATH','../../');
	require_once(SM_PATH . 'include/validate.php');
}
if (function_exists('nsm_bindtextdomain')) {
	nsm_bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
	nsm_textdomain('tbaddr');
} else {
	bindtextdomain('tbaddr',SM_PATH . 'plugins/tbaddr/locale');
	textdomain('tbaddr');
}

require_once(SM_PATH . 'plugins/tbaddr/functions.php');
tbaddr_options_init();
if(isset($_POST['submit'])) tbaddr_save();
tbaddr_loadprefs();
tbaddr_options();

if (function_exists('nsm_bindtextdomain')) {
	nsm_bindtextdomain('nasmail',SM_PATH . 'locale');
	nsm_textdomain('nasmail');
} else {
	bindtextdomain('squirrelmail',SM_PATH . 'locale');
	textdomain('squirrelmail');
}
