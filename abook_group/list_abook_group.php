<?php
/**
 * list_abook_group.php
 *
 * List abook groups for modify, delete or to list group members.
 * @copyright (c) 2002 Kelvin Ho
 * @copyright (c) 2002-2004 Jon Nelson <quincy at linuxnotes.net>
 * @copyright (c) 2004-2006 The SquirrelMail Project Team
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version $Id: list_abook_group.php,v 1.8 2006/09/13 16:36:42 tokul Exp $
 * @package sm-plugins
 * @subpackage abook_group
 */

/** @ignore */
if (!defined('SM_PATH'))define('SM_PATH','../../');

include_once(SM_PATH . 'include/validate.php');
include_once(SM_PATH . 'functions/addressbook.php');
include_once(SM_PATH . 'plugins/abook_group/abook_group_database.php');
include_once(SM_PATH . 'plugins/abook_group/abook_group_functions.php');

global $username;

/*  Start globals  */

//sqgetGlobalVar('username', $username, SQ_POST);
// we are not in mailbox
// sqgetGlobalVar('mailbox', $mailbox, SQ_POST);
sqgetGlobalVar('myparams', $myparams, SQ_POST);
sqgetGlobalVar('abookGroups', $abookGroups, SQ_POST);
sqgetGlobalVar('myGroups', $myGroups, SQ_POST);
sqgetGlobalVar('groupName', $groupName, SQ_POST);
sqgetGlobalVar('groupNameURL', $groupNameURL, SQ_POST);

/*  End globals  */

displayPageHeader($color, 'None');

/* Initialize addressbook */

$myparams = array();
$myparams['dsn'] = $addrbook_dsn;
$myparams['table'] = 'addressgroups';
$myparams['owner'] = $username;

$abookGroups = new abook_group_database($myparams);
$myGroups = $abookGroups->list_group();

?>

<table width="95%" align="center" cellpadding="2" cellspacing="2" border="0">
<tr><td bgcolor="<?php echo $color[0] ?>">
   <center><b>
<?php 
bindtextdomain('abook_group', SM_PATH . 'plugins/abook_group/locale');
textdomain('abook_group');
echo _("Groups");
bindtextdomain('squirrelmail', SM_PATH . 'locale');
textdomain('squirrelmail');
?>
</b></center>
</td></tr></table>
<?php
if (count($myGroups)>0){
?>
<table width="95%" align="center">
  <?php
for ($i=0;$i<count($myGroups);$i++){
    $groupName = $myGroups[$i]['addressgroup'];
    $groupNameURL = urlencode ($groupName);
?>
  <tr> 
    <td>
      <?php echo $groupName ?>
    </td>
    <?php 
      bindtextdomain('abook_group', SM_PATH . 'plugins/abook_group/locale');
      textdomain('abook_group');
      echo '<td><a href="modifyGroup.php?groupName=' . $groupNameURL . '">' . _("Modify") . '</a>'; 
      echo ' | <a href="modifyGroup.php?groupName=' . $groupNameURL . '&opt=delete">' . _("Delete") . '</a>'; 
      echo ' | <a href="listMembers.php?groupName=' . $groupNameURL . '">' .  _("List") . '</a></td>';
      bindtextdomain('squirrelmail', SM_PATH . 'locale');
      textdomain('squirrelmail');
    ?>
  </tr>
  <?php } ?>
</table>
<?php
} else {
?>
<p><center>
<?php 
bindtextdomain('abook_group', SM_PATH . 'plugins/abook_group/locale');
textdomain('abook_group');
echo _("You Currently Do Not Have Any Groups");
bindtextdomain('squirrelmail', SM_PATH . 'locale');
textdomain('squirrelmail');
?>
</center></p>
<?php
}
display_abook_group_footer();
?>
</body></html>
