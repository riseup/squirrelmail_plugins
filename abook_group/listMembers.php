<?php
/**
 * listMembers.php
 *
 * List Group members
 * @copyright (c) 2002 Kelvin Ho
 * @copyright (c) 2002-2004 Jon Nelson <quincy at linuxnotes.net>
 * @copyright (c) 2004-2006 The SquirrelMail Project Team
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version $Id: listMembers.php,v 1.12 2007/01/20 08:27:48 tokul Exp $
 * @package sm-plugins
 * @subpackage abook_group
 */

/** @ignore */
if (!defined('SM_PATH'))define('SM_PATH','../../');

include_once(SM_PATH . 'include/validate.php');
include_once(SM_PATH . 'functions/date.php');
include_once(SM_PATH . 'functions/display_messages.php');
include_once(SM_PATH . 'functions/addressbook.php');
include_once(SM_PATH . 'plugins/abook_group/abook_group_database.php');
include_once(SM_PATH . 'plugins/abook_group/abook_group_functions.php');


/* --- End functions --- */

//global $username;

/* Set globals */
//sqgetGlobalVar('username', $username, SQ_POST);
// we are not in mailbox
// sqgetGlobalVar('mailbox', $mailbox, SQ_POST);
//sqgetGlobalVar('myparams', $myparams, SQ_POST);
//sqgetGlobalVar('abook', $abook, SQ_POST);

sqgetGlobalVar('userData', $userData, SQ_POST);
sqgetGlobalVar('groupName', $groupName, SQ_GET);
//sqgetGlobalVar('myGroupMembers', $myGroupMembers, SQ_POST);
//sqgetGlobalVar('abookGroups', $abookGroups, SQ_POST);

sqgetGlobalVar('name', $name, SQ_POST);
sqgetGlobalVar('nickName', $nickName, SQ_POST);
sqgetGlobalVar('backendName', $backendName, SQ_POST);

/* remove operation vars */
sqgetGlobalVar('remove', $remove, SQ_POST);
sqgetGlobalVar('nickNameBackEnd', $nickNameBackEnd, SQ_POST);
// $groupname is already extracted
//sqgetGlobalVar('message', $message, SQ_POST);
/* End globals */

displayPageHeader($color, 'None');

/* Initialize addressbook */
$abook = addressbook_init(true,true);

$myparams = array();
$myparams['dsn'] = $addrbook_dsn;
$myparams['table'] = 'addressgroups';
$myparams['owner'] = $username;

$abookGroups = new abook_group_database($myparams);

if (isset($remove) && !empty($remove)){
    if (isset($nickNameBackEnd) && !empty($nickNameBackEnd)){
        $userData = explodeUserArray($nickNameBackEnd);
        $abookGroups->removeFromGroup($userData, $groupName);

        if ($abookGroups->error){
            $message = $abookGroups->error;
        } else{
            bindtextdomain('abook_group', SM_PATH . 'plugins/abook_group/locale');
            textdomain('abook_group');
            $message = _("Remove Successful");
            bindtextdomain('squirrelmail', SM_PATH . 'locale');
            textdomain('squirrelmail');
        }
    }
}

$myGroupMembers = $abookGroups->list_groupMembers($groupName);
?>

<table width="95%" align=center cellpadding=2 cellspacing=2 border=0>
<tr><td bgcolor="<?php echo $color[0] ?>">
   <center>
        <b> 
        <?php echo $groupName ?>&nbsp;
        <?php 
         bindtextdomain('abook_group', SM_PATH . 'plugins/abook_group/locale');
         textdomain('abook_group');
         echo _("Members");
         bindtextdomain('squirrelmail', SM_PATH . 'locale');
         textdomain('squirrelmail');
        ?>
        </b> 
      </center>
</td></tr></table>
<form name="form" method="post" action="<?php echo $PHP_SELF ?>">
        <?php
        if (isset($message)) { 
            echo '<p align="center"><b>' . $message . '</b></p>';
        }
        ?>
<?php 
if(is_array($myGroupMembers) && count($myGroupMembers)>0) { 
?>	
  <table width="95%" align="center">
    <?php
    for ($j=0;$j<count($myGroupMembers);$j++){
        $name = $myGroupMembers[$j]['name'];
        $nickName = $myGroupMembers[$j]['nickname'];
        $backendName = $abook->backends[$myGroupMembers[$j]['backend']]->bnum;

    ?>
<tr><td width="1%">

        <input type="checkbox" name="nickNameBackEnd[]" value="<?php echo $nickName.",".$backendName ?>">
      
    </td><td><?php echo $name ?></td></tr>
<?php
    }
?>
</table>
<?php
//end count > 0
} elseif (! empty($abookGroups->error)) {
     error_box($abookGroups->error,$color);
} else{
    bindtextdomain('abook_group', SM_PATH . 'plugins/abook_group/locale');
    textdomain('abook_group');
    echo "<p><center>"._("You Currently Do Not Have Any Members In This Group")."</center></p>";
    bindtextdomain('squirrelmail', SM_PATH . 'locale');
    textdomain('squirrelmail');
}
?>

  <p align="center">
    <input type="hidden" name="groupName" value="<?php echo $groupName ?>">
    <input type="submit" name="remove" value="
       <?php 
        bindtextdomain('abook_group', SM_PATH . 'plugins/abook_group/locale');
        textdomain('abook_group');
        echo _("Remove");
        bindtextdomain('squirrelmail', SM_PATH . 'locale');
        textdomain('squirrelmail');
       ?>
    ">
  </p>
</form>
<?php display_abook_group_footer(); ?>
</body></html>
