<?php

/**
  * SquirrelMail Compose Extras Plugin
  * Copyright (c) 2003 Bruce Richardson <itsbruce@uklinux.net>
  * Copyright (c) 2003-2004 Justus Pendleton <justus@ryoohki.net>
  * Copyright (c) 2005-2009 Paul Lesniewski <paul@squirrelmail.org>
  * Licensed under the GNU GPL. For full terms see the file COPYING.
  *
  * @package plugins
  * @subpackage compose_extras
  *
  */

global $ce_limit_submit, $ce_disable_recipient_fields,
       $allow_acceses_keys;



// Should the plugin attempt to limit the user's ability to click more
// than once to send an email?
//
//    0 = no
//    1 = yes
//
$ce_limit_submit = 1; 



// Should users be forced to use the address book to add
// recipients?  If this is enabled, users won't be able to
// type anything directly into the To, Cc and Bcc fields
//
//    0 = no
//    1 = yes
//
$ce_disable_recipient_fields = 0;



// Should users be allowed to have access keys on the
// compose screen?
//
//    0 = no
//    1 = yes
//
$allow_acceses_keys = 1; 



