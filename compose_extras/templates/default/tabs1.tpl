<?php

/**
  * tabs1.tpl
  *
  * Template for outputting JavaScript that reorganized the
  * tab order on the compose screen.
  *
  * The following variables are available in this template:
  *
  * string  submit_button_name  The name of the send button
  *
  * @copyright &copy; 1999-2009 The SquirrelMail Project Team
  * @license http://opensource.org/licenses/gpl-license.php GNU Public License
  * @version $Id$
  * @package squirrelmail
  * @subpackage plugins
  */


// retrieve the template vars
//
extract($t);


?>
<script language='JavaScript' type='text/javascript'>
    document.compose.send_to.tabIndex=1;
    document.compose.send_to_cc.tabIndex=5;
    document.compose.send_to_bcc.tabIndex=6;
    document.compose.subject.tabIndex=2;
    document.compose.body.tabIndex=3;
    document.compose.<?php echo $submit_button_name; ?>.tabIndex=4;
</script>

