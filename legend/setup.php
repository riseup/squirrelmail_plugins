<?php
/*
 * Highlight Legend plugin v1.2 by Thijs Kinkhorst
 *
 * Note: this plugin only works with SM >= 1.2.8 OR php >= 4.1.
 *
 * last modified: 2002/08/05 by Thijs Kinkhorst
 * please send comments and bug reports to <thijs@kinkhorst.com>
 *
 */
function squirrelmail_plugin_init_legend ()
{
    global $squirrelmail_plugin_hooks;

    $squirrelmail_plugin_hooks['left_main_after']['legend'] = 'plugin_legend_display';
    $squirrelmail_plugin_hooks['options_highlight_bottom']['legend'] = 'plugin_legend_options';
}


function plugin_legend_display () {

   global $message_highlight_list, $data_dir, $username;
   
   if(getPref($data_dir, $username, 'highl_legend') && count($message_highlight_list) > 0)
   {
       echo '<p><table width="80%" align="center" cellpadding="3" cellspacing="1" border="0">';
   
       foreach($message_highlight_list as $item)
       {
           echo '<tr bgcolor="#' . $item['color']
                . '"><td><small>&nbsp;' . $item['name']
                . '</small></td></tr>' . "\n";
       }
    
       echo '</table></p>';
   }
}

function plugin_legend_options () {

    global $data_dir, $username, $PHP_SELF;
    $legend = $_GET['legend'];

    if($legend == 'on') {
        setPref($data_dir, $username, 'highl_legend', 'on');
        $notlegend = 'off';
        $changed = 1;
    }
    elseif($legend == 'off') {
        removePref($data_dir, $username, 'highl_legend');
        $notlegend = 'on';
        $changed = 1;
    }
    elseif(getPref($data_dir, $username, 'highl_legend')) {
        $legend = 'on';
        $notlegend = 'off';
    }
    else {
        $legend = 'off';
        $notlegend = 'on';
    }

    echo '<P align="center">Legend below folder list is now: <B>'
       . ucfirst($legend)
       . '</B> (<A HREF="' . $_SERVER['PHP_SELF'] . '?legend='
       . $notlegend . '">turn it '
       . $notlegend . '</A>)';

    if($changed) {
        echo ' [<A HREF="../src/left_main.php" TARGET="left">'
            . _("Refresh Folder List") . '</A>]';
    }
    echo '</P>';
    
}
?>
