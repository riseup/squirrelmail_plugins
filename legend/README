Highlight Legend plugin v1.2


Description
===========

This plugin adds a legend below the folder list indicating the meaning of each
highlighting color as defined by the user. This legend is a table with on each
line the name of a highlighting rule and the corresponding background color.
The display of this legend is configurable through the Options - Message
Highlighting page.

Note: this plugin requires either a PHP version >= 4.1 OR SquirrelMail >= 1.2.8.


Installation
============

As with other plugins, just uncompress the archive in the plugins
directory, go back to the main directory, run configure and add the plugin.
Turn the legend on at the Options - Message Highlighting page.

Questions/comments/flames/etc can be sent to
    Thijs Kinkhorst <thijs@kinkhorst.com>


Changes
=======
1.0
 * Initial version
1.1
 * Fix bug with preferences. This plugin now uses a different preference
   name so you will have to turn it on again if you've used v1.0.
 * Create some more whitespace between the folderlist and the legend.
1.1a
 * Fix bug which only occurs when using Netscape 4.7x
1.2
 * Make the plugin register_globals=off compatible.


Acknowledgements
================
This plugin was created for Jacob Kleerekoper <jacob@A-Eskwadraat.nl>.
